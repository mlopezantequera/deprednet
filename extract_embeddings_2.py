#!/usr/bin/env python3

import os
import argparse
import json
import h5py
import numpy as np
import tensorflow as tf
import functools
from skimage.transform import resize
from imageio import imread
from time import time

from network import networkDefiner
import Preprocessing_Factory.preprocessing_factory as preprocessor
import inputDatasets
dir(tf.contrib) # github.com/tensorflow/tensorflow/issues/10130

"""

This file loads builds a network using a network definition (.py) file through the network.py interface and the
net factory.

TO-DO: preprocessing should be done using the preprocessing factories available in tf.slim (or a new one for a given net)
[ Right now I'm hardcoding VGGnet preprocessing + direct resize without cropping, watch out for this! ]

"""

#vggnet mean
_R_MEAN = 123.68
_G_MEAN = 116.78
_B_MEAN = 103.94

def read_images(*args):
  #this should be replaced by the preprocessing factory so it is network independent.
  #also, the preprocessing factory is intended to be used with the Dataset pipeline (which would be ideal)
  return read_images_hardcoded_semi_vggnet_preprocessing(*args)

def read_images_hardcoded_semi_vggnet_preprocessing(image_dir, image_paths, image_height, image_width, startix, endix, batch_array):
  #true vggnet preprocessing includes crops. this is just a resize plus mean removal.
  ix = 0
  for file in image_paths[startix:endix]:
    image_file = os.path.join(image_dir, file)
    assert os.path.exists(image_file), "File does not exist: %s" % image_file
    image = imread(image_file)
    batch_array[ix, ...] = resize(image, (image_height, image_width), mode='edge', preserve_range=True) - np.array((_R_MEAN, _G_MEAN, _B_MEAN))
    ix += 1
  return batch_array

def do_dset(net_file_path, index_fp, output_layer=None):
  batch_size = 10

  config = tf.ConfigProto(log_device_placement=False)
  config.gpu_options.allow_growth = True
  sess = tf.Session(config=config)

  with open(net_file_path, 'r+') as f:
    net_params = json.load(f)
  print(net_params)
  net_params['is_training'] = False
  if output_layer:
    net_params['output_layer'] = output_layer

  dset_name = index_fp.split('/')[-1].split('.')[0]
  # weightsname = os.path.split(weights_fp)[1]
  output_fp = "/".join(index_fp.split('/')[:-1]) + '/' + dset_name + '-' + net_params['net_type'] + '_' + net_params[
    'output_layer'].replace('/', '-') + '.h5'

  batching_params = {'batch_size': batch_size, 'buffer_val': 500, 'threads_val': 8}

  image_preprocessing_eval = preprocessor.get_preprocessing(net_params['net_type'], is_training=False)

  func_imgproc_val = functools.partial(image_preprocessing_eval, output_height=net_params['image_height'],
                                       output_width=net_params['image_width'])  # is_training=False)

  paths_indices = [index_fp]
  preprocessing_functions = [func_imgproc_val]
  phases = ['val']

  next_batch, handle, dataset_dicts = inputDatasets.createdatasets(paths_indices, batching_params,
                                                                   preprocessing_functions, phases)


  validation_iterator = dataset_dicts[0]['iterator']
  validation_iterator_initializer = dataset_dicts[0]['initializer']
  validation_handle = sess.run(validation_iterator.string_handle())
  #eff_len_val = dataset_dicts[0]['length']
  #eval_poses = np.empty((eff_len_val, 7))  # x,y,z,qw,qx,qy,qz

  batch_images = tf.reshape(next_batch[0],
                            (batch_size, net_params['image_height'], net_params['image_width'], 3))
  network_vars, load_ops, load_dicts = networkDefiner(batch_images, net_params)

  print("Loading weights from " + net_params['checkpoint_path'])
  for load_op, load_dict in zip(load_ops, load_dicts):
    sess.run(load_op, feed_dict=load_dict)

  print("Output tensor:", network_vars['embeddings'])

  embeddings = tf.contrib.layers.flatten(network_vars['embeddings'])

  if net_params['l2_normalize_embeddings']:
    print("L2-normalizing the embeddings")
    output_fp = output_fp.replace('.h5', '-L2.h5')
    embeddings = tf.nn.l2_normalize(embeddings, dim=1)

  outdim = int(embeddings.shape[1])
  batch_size = int(embeddings.shape[0])
  image_height = int(batch_images.shape[1])
  image_width = int(batch_images.shape[2])
  batch_images_val = np.zeros((batch_size, image_height, image_width, 3))

  with open(index_fp, 'r') as f:
    d = json.load(f)

  image_root = os.path.join(os.path.split(index_fp)[0], d['im_prefix'])
  image_paths = d['im_paths'][:]
  len_dataset = len(image_paths)
  full_batches_in_dataset = len_dataset // batch_size
  leftover_imgs = len_dataset - full_batches_in_dataset * batch_size

  with h5py.File(output_fp, 'w') as outf:
    outf.create_dataset('features', (len_dataset, outdim), 'float32')
    sess.run(validation_iterator_initializer)
    batch_eval = 0
    while True:  # all the validation dataset
      try:
        s_ix = batch_eval * batch_size
        e_ix = (batch_eval + 1) * batch_size
        if e_ix > len_dataset:
          e_ix = len_dataset
        t0 = time()
        outf['features'][s_ix:e_ix, ...] = sess.run((embeddings[0:e_ix-s_ix, ...]), {handle: validation_handle})
        t1 = time()
        t_load = t1 - t0
        batch_eval += 1
        if batch_eval % 10 == 0:
          print("Batch {}/{} ({:.2%}), pass: {:.1f} ms/im".format(
            batch_eval, full_batches_in_dataset, batch_eval / full_batches_in_dataset, 1000 * t_load / batch_size))

      except tf.errors.OutOfRangeError:
        break
  print("Done!, saved output in: {}".format(output_fp))
  sess.close()


if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('net_params_file', help='Json with the network params')
  parser.add_argument('path_dataset_index', help='Path to a .json file for the dataset')
  parser.add_argument('-out_t', help='Name of the output tensor', default=None)
  args = parser.parse_args()

  do_dset(args.net_params_file, args.path_dataset_index, output_layer=args.out_t)
