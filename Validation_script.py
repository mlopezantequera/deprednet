#!/usr/bin/env python3

# SBATCH --job-name=depred
# SBATCH --time=24:00:00
# SBATCH --mem=64000
# SBATCH --nodes=1

import numpy as np
import time
import os
import json
import h5py
import tensorflow as tf
from scipy.spatial.distance import pdist, squareform, cdist
from sklearn.metrics.ranking import precision_recall_curve
from sklearn.metrics.ranking import average_precision_score

home = os.path.expanduser('~')

import functools
import transformations as tr
import matplotlib

matplotlib.use('agg')
import matplotlib.pyplot as plt

from extract_embeddings import do_dset
from inputDatasets import read_dataset, quat_dist
import extra


# val_params['validation_sets']
def extract_embeddings_from_graph(snapshot_file, validation_set):
  print('FOR SNAPSHOT: ' + snapshot_file.split('.')[0] + '\n')
  print('    - Validating ' + validation_set)
  do_dset(snapshot_file.split('.')[0], validation_set)
  tf.reset_default_graph()


# os.path.join(val_params['snapshot_dir'], 'validations')
def show(confmat, P, R, GT, ap, path, valdat_name, Thresholds=None, do_ap=False):
  # Save the confmat as an image
  plt.clf()
  plt.imshow(confmat)
  plt.colorbar()
  plt.savefig(os.path.join(path, valdat_name + '_confmat.jpg'))

  # Save the GT as an image
  plt.clf()
  plt.imshow(GT, interpolation='hamming')
  plt.savefig(os.path.join(path, valdat_name + '_GT.jpg'))

  if (Thresholds is None) or do_ap:
    # Save the precision-recall curve
    plt.figure()
    extra.plot_pr_curve(R, P)
    plt.title("VAL, AP: {}".format(ap))
    plt.savefig(os.path.join(path, valdat_name + '_prc.jpg'))

  if Thresholds:
    f1_score = 2 * (P * R) / (P + R)
    t_max = -Thresholds[np.argmax(f1_score)]
    plt.clf()
    plt.title('Threshold max: ' + str(t_max))
    plt.imshow(confmat < t_max, interpolation='hamming')
    plt.colorbar()
    plt.savefig(os.path.join(path, valdat_name + '_bin_confmat.jpg'))
  plt.close('all')


def show_comparation(val_params, val_dicts, N=5, write_aps_doc=True):
  keys = list(val_dicts.keys())
  keys = [keys[i:i + N] for i in range(0, len(keys), N)]
  for i, k in enumerate(keys):
    # Save the precision-recall curve
    plt.figure()
    plt.xlabel('precision')
    plt.ylabel('recall')
    plt_list = []
    for subk in k:
      f = extra.plot_pr_curve(val_dicts[subk]['R'], val_dicts[subk]['P'],
                   label=subk.split('-')[0] + ':' + '{:.4f}'.format(val_dicts[subk]['ap']))
      plt_list.append(f)
    plt.legend()
    plt.savefig(os.path.join(val_params['eval_dir'], 'validations',
                             str(i) + '_prc_' + '-'.join(k[0].split('-')[1:]) + '-T' + str(
                               val_params['tol_t']) + '_R' + str(val_params['tol_r']) + '.jpg'))
    plt.close()

  if write_aps_doc:
    with open(os.path.join(val_params['eval_dir'], 'validations',
                           'aps' + '-'.join(keys[0][0].split('-')[1:]) + '-T' + str(val_params['tol_t']) + '_R' + str(
                               val_params['tol_r']) + '.txt'), 'w+') as f:
      f.write('\n'.join([x + ' = ' + str(val_dicts[x]['ap']) for x in val_dicts.keys()]))

def normalize_rows(confmat, num_rows=None):
  '''
  Normalize rows in order to have a useful confmat. The procedure depends on the form of the confmat (matrix or distance
  vector)
  :param confmat: pdist/cdist confmat
  :return: normalized confmat
  '''
  if confmat.ndim == 1:
    # Normalize rows (here the confmat is a vector-form distance vector, see scipy.spatial.distance.squareform), so each
    # row must be normalized separately extracting it from the distance vector
    if num_rows is None:
      raise ValueError("The number of rows must be known")
    prev = 0
    for i in range(num_rows):
      row = num_rows * (num_rows - 1) // 2 - (num_rows - i) * (num_rows - i - 1) // 2 + num_rows - i - 1
      confmat[prev:row] = confmat[prev:row] / np.max(confmat[prev:row])
      if row - prev == 1:
        break
      else:
        prev = row
  else:
    # Normalize rows (here the confmat is 2dim)
    confmat = confmat / np.amax(confmat, axis=1, keepdims=True)
  return confmat

def validate_datasets(val_params):
  '''
  1 - datasets_simple: evaluate one model in various datatasets, but without crossing them
                       model = list len=1  datasets = list
        "mode": 1,
  "validation_sets":
  ["/mnt/data/datasets/nordlandsbanen_227x227/summer_all_no_dark_ss-100.json",
  "/mnt/data/datasets/nordlandsbanen_227x227/winter_all_no_dark_ss-100.json",
  "/mnt/data/datasets/nordlandsbanen_227x227/fall_all_no_dark_ss-100.json",
  "/mnt/data/datasets/nordlandsbanen_227x227/spring_all_no_dark_ss-100.json"],
  "embeddings":
    "[ "/mnt/data/datasets/nordlandsbanen_227x227/summer_all_no_dark_ss-100-nasnet_mobile_Cell_0.h5",
    "/mnt/data/datasets/nordlandsbanen_227x227/winter_all_no_dark_ss-100-nasnet_mobile_Cell_0.h5"
    "/mnt/data/datasets/nordlandsbanen_227x227/fall_all_no_dark_ss-100-nasnet_mobile_Cell_0.h5"
    "/mnt/data/datasets/nordlandsbanen_227x227/spring_all_no_dark_ss-100-nasnet_mobile_Cell_0.h5"],
 "crossed": True
  '''

  dataset_names = [x.split('/')[-1].split('.')[0] for x in val_params['validation_sets']]

  if val_params['cross_evaluation']:
    evaluations = list(set([tuple(sorted((x, y))) for x in dataset_names for y in dataset_names]))
  else:
    evaluations = [(x, x) for x in dataset_names]

  embeddings = val_params['embeddings']
  tol_quat = 2 * (1 - np.dot(tr.quaternion_about_axis(val_params['tol_r'] * np.pi / 180, (0, 0, 1)), (1, 0, 0, 0)))

  for evaluation in evaluations:
    valdat_name = '_'.join(list(evaluation)) + '-T' + str(val_params['tol_t']) + '_R' + str(val_params['tol_r'])
    print('Evaluating ' + valdat_name)

    with h5py.File([x for x in embeddings if evaluation[0] in x][0], 'r') as outf:
      eval_embeddings = np.array(outf['features'])
    _, eval_poses = read_dataset([x for x in val_params['validation_sets'] if evaluation[0] in x][0], {})

    if evaluation[0] != evaluation[1]:
      with h5py.File([x for x in embeddings if evaluation[1] in x][0], 'r') as outf:
        eval_embeddings_2 = np.array(outf['features'])
      _, eval_poses_2 = read_dataset([x for x in val_params['validation_sets'] if evaluation[1] in x][0], {})

    # Here the validation is result of crossing between the validation sets
    ds_factor = 1
    if evaluation[0] != evaluation[1]:
      confmat_2dim = cdist(eval_embeddings[::ds_factor], eval_embeddings_2[::ds_factor])
      if val_params["normalize_rows"]:
        confmat_2dim = normalize_rows(confmat_2dim)
      eucdist_2dim = cdist(eval_poses[::ds_factor, :3], eval_poses_2[::ds_factor, :3])
      quatdist_2dim = quat_dist(eval_poses[::ds_factor, 3:], eval_poses_2[::ds_factor, 3:])
      np.fill_diagonal(quatdist_2dim, 0.0)
      confmat = confmat_2dim
      eucdist = eucdist_2dim
      quatdist = quatdist_2dim
      print(confmat.shape, eucdist.shape)
    else:
      confmat = pdist(eval_embeddings[::ds_factor])
      if val_params["normalize_rows"]:
        confmat = normalize_rows(confmat, num_rows=len(eval_embeddings))
      eucdist = pdist(eval_poses[::ds_factor, :3])
      quatdist = quat_dist(eval_poses[::ds_factor, 3:], eval_poses[::ds_factor, 3:])
      np.fill_diagonal(quatdist, 0.0)
      quatdist = squareform(quatdist)
      confmat_2dim = squareform(confmat)  # convert the compact distance matrix into a square matrix for display

    '''with h5py.File(os.path.join(val_params['eval_dir'], 'validations', valdat_name + '_data.h5'), 'w') as outf:
      outf['confmat'] = confmat
      outf['eucdist'] = eucdist
      outf['quatdist'] = quatdist'''

    print("eval: calculating PR curve and AP")
    GT = np.logical_and(eucdist < val_params['tol_t'], quatdist < tol_quat)  # our ground truth
    P, R, _ = precision_recall_curve(GT.flatten(), -1 * confmat.flatten())  # reverse confmat to get a 'similarity score'
    average_precision = average_precision_score(GT.flatten(), -1 * confmat.flatten())

    if evaluation[0] == evaluation[1]:
      GT_2dim = squareform(GT)
    else:
      GT_2dim = GT

    if P.shape[0] > 50000:
      ixs_PR = np.linspace(0, P.shape[0] - 1, num=50000, dtype=np.int)
    else:
      ixs_PR = np.arange(P.shape[0])
    P = P[ixs_PR]
    R = R[ixs_PR]

    show(confmat_2dim, P, R, GT_2dim, average_precision, os.path.join(val_params['eval_dir'], 'validations'), valdat_name, do_ap=True)


def validate_from_embeddings(val_params):
  '''
  2 - multiple_embeddings: evaluate various models in one dataset
                       model = list  datasets = list len=1
    "mode": 2,
"validation_sets":
  "/mnt/data/datasets/nordlandsbanen_227x227/summer_all_no_dark_ss-100.json",
"embeddings":
    "[ "/mnt/data/datasets/nordlandsbanen_227x227/summer_all_no_dark_ss-100-nasnet_mobile_Cell_0.h5",
    "/mnt/data/datasets/nordlandsbanen_227x227/summer_all_no_dark_ss-100-nasnet_mobile_Cell_1.h5",
    ...],
  '''
  val_params = {}
  val_params['eval_dir'] = '/mnt/data/datasets/kitti_odometry/'
  val_params['validation_set'] = '/mnt/data/datasets/kitti_odometry/00_left_ss-20.json'
  val_params['tol_t'] = 10
  val_params['tol_r'] = 5

  embeddings = val_params['embeddings']
  tol_quat = 2 * (1 - np.dot(tr.quaternion_about_axis(val_params['tol_r'] * np.pi / 180, (0, 0, 1)), (1, 0, 0, 0)))

  val_dicts = {}
  for embedding in embeddings:
    val = dict()
    print('\nEvaluating ' + embedding)

    with h5py.File(embedding, 'r') as outf:
      eval_embeddings = np.array(outf['features'])

    _, eval_poses = read_dataset(val_params['validation_set'], {})

    # Here the validation is result of crossing between the validation sets
    ds_factor = 1
    confmat = pdist(eval_embeddings)[::ds_factor]
    if val_params["normalize_rows"]:
      confmat = normalize_rows(confmat, num_rows=len(eval_embeddings))
    eucdist = pdist(eval_poses[::ds_factor, :3])
    quatdist = quat_dist(eval_poses[::ds_factor, 3:], eval_poses[::ds_factor, 3:])
    np.fill_diagonal(quatdist, 0.0)
    quatdist = squareform(quatdist)
    confmat_2dim = squareform(confmat)  # convert the compact distance matrix into a square matrix for display

    print("eval: calculating PR curve and AP")
    GT = np.logical_and(eucdist < val_params['tol_t'], quatdist < tol_quat)  # our ground truth
    P, R, _ = precision_recall_curve(GT, -1 * confmat)  # reverse confmat to get a 'similarity score'
    average_precision = average_precision_score(GT, -1 * confmat)

    GT_2dim = squareform(GT)
    np.fill_diagonal(GT_2dim, True)

    if P.shape[0] > 50000:
      ixs_PR = np.linspace(0, P.shape[0] - 1, num=50000, dtype=np.int)
    else:
      ixs_PR = np.arange(P.shape[0])
    P = P[ixs_PR]
    R = R[ixs_PR]

    val['P'] = P
    val['R'] = R
    # val['GT'] = GT
    val['ap'] = average_precision
    val_dicts[embedding] = val

    show(confmat_2dim, P, R, GT_2dim, average_precision, os.path.join(val_params['eval_dir'], 'validations'), embedding)
  show_comparation(val_params, val_dicts)


def validate_embeddings_datasets(val_params):
  '''    3 - embeddings: evaluate various models in various datasets, creating the frame
                       model = dict->keys=datasets   datasets = list
                       {
"mode": 3,
"validation_sets": [
  "/mnt/data/datasets/nordlandsbanen_227x227/summer_all_no_dark_ss-100.json",
  "/mnt/data/datasets/nordlandsbanen_227x227/winter_all_no_dark_ss-100.json"
],
"embeddings": {
  "summer_all_no_dark_ss-100": [
    "/mnt/data/datasets/nordlandsbanen_227x227/summer_all_no_dark_ss-100-nasnet_mobile_Cell_0.h5",
    "/mnt/data/datasets/nordlandsbanen_227x227/summer_all_no_dark_ss-100-nasnet_mobile_Cell_1.h5",
    ...],
  "winter_all_no_dark_ss-100": [
    "/mnt/data/datasets/nordlandsbanen_227x227/winter_all_no_dark_ss-100-nasnet_mobile_Cell_0.h5",
    "/mnt/data/datasets/nordlandsbanen_227x227/winter_all_no_dark_ss-100-nasnet_mobile_Cell_1.h5"
    ...]
    },
  '''

  embeddings = val_params['embeddings']
  dataset_names = [x.split('/')[-1].split('.')[0] for x in val_params['validation_sets']]

  if list(embeddings.keys()).sort() != dataset_names.sort():
    raise ValueError('MUST BE THE SAME:\n' + str(embeddings.keys()) + '\n' + str(dataset_names))

  if val_params['cross_evaluation']:
    evaluations = list(set([tuple(sorted((x, y))) for x in dataset_names for y in dataset_names if x != y]))
  else:
    evaluations = [(x, x) for x in dataset_names]

  tol_quat = 2 * (1 - np.dot(tr.quaternion_about_axis(val_params['tol_r'] * np.pi / 180, (0, 0, 1)), (1, 0, 0, 0)))

  for evaluation in evaluations:
    val_dicts = {}
    valdat_name = '_'.join(list(evaluation)) + '-T' + str(val_params['tol_t']) + '_R' + str(val_params['tol_r'])
    print('\n-Evaluating datasets: ' + valdat_name)

    embeddings_names = [x.split('/')[-1].split('.')[0].split('-')[-1] for x in embeddings[evaluation[0]]]

    _, eval_poses = read_dataset([x for x in val_params['validation_sets'] if evaluation[0] in x][0], {})
    _, eval_poses_2 = read_dataset([x for x in val_params['validation_sets'] if evaluation[1] in x][0], {})

    for emb_name in embeddings_names:
      print('Evaluating embedding ' + emb_name)
      val = {}
      with h5py.File([x for x in embeddings[evaluation[0]] if emb_name in x][0], 'r') as outf:
        eval_embeddings = np.array(outf['features'])
      with h5py.File([x for x in embeddings[evaluation[1]] if emb_name in x][0], 'r') as outf:
        eval_embeddings_2 = np.array(outf['features'])

      # Here the validation is result of crossing between the validation sets
      ds_factor = 1
      if evaluation[0] != evaluation[1]:
        confmat_2dim = cdist(eval_embeddings[::ds_factor], eval_embeddings_2[::ds_factor])
        if val_params["normalize_rows"]:
          confmat_2dim = normalize_rows(confmat_2dim)
        eucdist_2dim = cdist(eval_poses[::ds_factor, :3], eval_poses_2[::ds_factor, :3])
        quatdist_2dim = quat_dist(eval_poses[::ds_factor, 3:], eval_poses_2[::ds_factor, 3:])
        np.fill_diagonal(quatdist_2dim, 0.0)
        confmat = confmat_2dim.flatten()
        eucdist = eucdist_2dim
        quatdist = quatdist_2dim
      else:
        confmat = pdist(eval_embeddings)[::ds_factor]
        if val_params["normalize_rows"]:
          confmat = normalize_rows(confmat, num_rows=len(eval_embeddings))
        eucdist = pdist(eval_poses[::ds_factor, :3])
        quatdist = quat_dist(eval_poses[::ds_factor, 3:], eval_poses[::ds_factor, 3:])
        np.fill_diagonal(quatdist, 0.0)
        quatdist = squareform(quatdist)
        confmat_2dim = squareform(confmat)  # convert the compact distance matrix into a square matrix for display

      print("eval: calculating PR curve and AP\n")
      GT = np.logical_and(eucdist < val_params['tol_t'], quatdist < tol_quat)  # our ground truth
      P, R, _ = precision_recall_curve(GT.flatten(), -1 * confmat)  # reverse confmat to get a 'similarity score'
      average_precision = average_precision_score(GT, -1 * confmat)

      if evaluation[0] == evaluation[1]:
        GT_2dim = squareform(GT)
      else:
        GT_2dim = GT
      np.fill_diagonal(GT_2dim, True)

      if P.shape[0] > 50000:
        ixs_PR = np.linspace(0, P.shape[0] - 1, num=50000, dtype=np.int)
      else:
        ixs_PR = np.arange(P.shape[0])
      P = P[ixs_PR]
      R = R[ixs_PR]

      val['P'] = P
      val['R'] = R
      # val['GT'] = GT
      val['ap'] = average_precision
      val_dicts['-'.join((emb_name,) + evaluation)] = val
      show(confmat_2dim, P, R, GT_2dim, average_precision, os.path.join(val_params['eval_dir'], 'validations'),
           '-'.join((emb_name,) + evaluation))

    show_comparation(val_params, val_dicts)


def main(_):
  '''
  1 - datasets_simple: evaluate one model in various datatasets, but without crossing them
                       model = list len=1  datasets = list
  1 - datasets_crossed: evaluate one model in various datasets, creating a frame that compares all datasets ('cross_evaluation')
                       model = list len=1  datasets = list
  2 - multiple_embeddings: evaluate various models in one dataset
                       model = list  datasets = list len=1
  3 - embeddings: evaluate various models in various datasets, creating the frame
                       model = dict->keys=datasets   datasets = list
  '''
  # validate_from_embeddings()

  with open('./validation_params.json', 'r+') as f:
    val_params = json.load(f)

  if not val_params["validate_subfolders"]:
    eval_dirs = [val_params['eval_dir']]
    snapshot_dirs = [val_params['snapshot_dir']]
  else:
    eval_dirs = [os.path.join(val_params['eval_dir'], x) for x in os.listdir(val_params['eval_dir']) if
                 os.path.isdir(os.path.join(val_params['eval_dir'], x))]
    eval_dirs = [x for x in eval_dirs if 'val' in os.listdir(x)]
    snapshot_dirs = eval_dirs

  for ev_dir, sn_dir in zip(eval_dirs, snapshot_dirs):
    val_params['eval_dir'] = ev_dir
    val_params['snapshot_dir'] = sn_dir

    os.makedirs(os.path.join(val_params['eval_dir'], 'validations'), exist_ok=True)
    # todo: conversion from ckpt to embedding if necessary. MAYBE: select type of embedding and create them

    if val_params['create_embeddings']:
      '''
      If true the embeddings params can be an empty list
      '''
      snapshot_file = [os.path.join(val_params['snapshot_dir'], x) for x in os.listdir(val_params['snapshot_dir']) if 'meta' in x][0]
      for validation_set in val_params['validation_sets']:
          extract_embeddings_from_graph(snapshot_file, validation_set)
      val_params['embeddings'] = [os.path.join(val_params['snapshot_dir'], x) for x in os.listdir(val_params['snapshot_dir']) if '.h5' in x]

    if val_params['mode'] == 1:
      validate_datasets(val_params)
    elif val_params['mode'] == 2:
      validate_from_embeddings(val_params)
    elif val_params['mode'] == 3:
      validate_embeddings_datasets(val_params)
    else:
      raise ValueError

if __name__ == '__main__':
  tf.app.run()