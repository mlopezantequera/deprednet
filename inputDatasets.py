"""

Input pipeline using the Dataset API

"""

import json
import os
import random
import numpy as np
import tensorflow as tf

import scipy
import scipy.misc
import functools
import pickle

from tensorflow.contrib.data import Dataset, Iterator
from scipy.spatial.distance import squareform

import transformations as tr
import image_processing

def ceildiv(a, b):
    return -(-a // b)

def quat_dist(q1, q0):
  """
  Rotation distance using quaternions. Robust to non-sign-normalized quaternions.
  :param q1: array of size (N, 4)
  :param q2: array of size (N, 4)
  :return: array of size (N,N)
  """
  return (1 - (np.square(np.dot(q1, np.transpose(q0)))))


def matrix_to_xyzquat(poses):
  """
  :param poses: N x 12 vectors representing a transformation matrix
  :return: N x 7 vectors in xyz + quaternion (x, y, z, qw, qx, qy, qz) format.
  """
  bs = poses.shape[0]
  xyzquat = np.empty((bs, 7), dtype=np.float32)
  for ixa in range(bs):
    m = poses[ixa, :].reshape(3, 4)
    qa = tr.quaternion_from_matrix(m[:, :3])
    xyzquat[ixa, :3] = m[:, 3]
    xyzquat[ixa, 3:] = qa
  return xyzquat


def latlonyaw_to_xyzquat(poses):
  """Transforms poses

  :param poses: array of N x 6 (lat, lon, alt, roll, pitch, yaw)
  :return: array of N x 7 (x, y, z, qw, qx, qy, qz)

  lat:   latitude (deg)
  lon:   longitude (deg)
  alt:   altitude (m)
  roll:  roll angle (rad),    0 = level, positive = left side up,      range: -pi   .. +pi
  pitch: pitch angle (rad),   0 = level, positive = front down,        range: -pi/2 .. +pi/2
  yaw:   heading (rad),       0 = east,  positive = counter clockwise, range: -pi   .. +pi

  And outputs:
  x, y, z, qw, qx, qy, qz
  xyz are projected onto a sphere the size of earth

  The quaternion is simply a conversion from the yaw angle,
  it is not properly projected onto earths surface.
  (but it works for our purposes, as they are only used by comparing them to each other)

  """
  r = 6371000.0 #Approximate radius of earth
  out = np.empty([len(poses), 7], dtype=np.float32)
  lat = poses[:,0] * np.pi / 180
  lon = poses[:,1] * np.pi / 180
  yaw = poses[:,5]
  out[:, 0] = r * np.cos(lat) * np.cos(lon)
  out[:, 1] = r * np.cos(lat) * np.sin(lon)
  out[:, 2] = r * np.sin(lat)
  for ix in range(len(poses)):
    out[ix, 3:] = tr.quaternion_about_axis(yaw[ix], (0,0,1))
  return out

def figure_out_pose_format(d):
  keys = d.keys()
  if 'poses' in keys:
    poses = d['poses']
  elif 'metric_poses' in keys:
    poses = d['metric_poses']
  elif 'ids' in keys:
    poses = d['ids']
  else:
    raise ValueError

  pose_len = len(poses[0])

  if pose_len == 1:
    pose_format = 'id'
  elif pose_len == 6:
    pose_format = 'lat_lon_alt_pitch_roll_yaw'  # geolocated datasets with absolute poses: lat,lon,alt,pitch,roll,yaw (like cityscapes)
  elif pose_len == 12:
    pose_format = 'matrix'
  elif pose_len == 7:
    # poses already in xyzquat, but we don't know if it is xyzw or wxyz
    raise ValueError("Unknown poses format with length:", pose_len, "is it quat_xyzw or quat_wxyz?")
  else:
    raise ValueError("Unknown poses format with length:", pose_len)
  print("Assuming that poses of length {} are of type: {}".format(pose_len, pose_format))
  return pose_format, poses

def adapt_poses(in_poses, pose_format = None, min_t_distant = None):
  in_poses = np.asarray(in_poses, dtype=np.float32)
  if pose_format == 'id':
    # datasets without actual pose information, where the 'pose' is simply an integer ID for the location. (AMOS, alderley)
    # we just give them fake poses ensuring they show up correctly as neighbors or not in all the rest of the code.
    # warning: if we ever want to mix different datasets in the same minibatch, the fake poses must be different!
    pose_id = in_poses
    poses = np.zeros((pose_id.shape[0], 7), dtype=np.float32)
    poses[:, 3] = 1  # All poses have the same orientation quaternion (1,0,0,0)
    poses[:, 2] = pose_id * (
      2 * min_t_distant)  # z coordinate is set to the image ID multiplied by twice the distant threshold.
  elif pose_format == 'lat_lon_alt_pitch_roll_yaw':
    # geolocated datasets with absolute poses: lat,lon,alt,pitch,roll,yaw (like cityscapes)
    poses = latlonyaw_to_xyzquat(in_poses)
  elif pose_format == 'matrix':
    # datasets with relative poses given by their transformation matrix (like kitti-odometry)
    poses = matrix_to_xyzquat(in_poses)
  elif pose_format == 'quat_xyzw':
    # poses already in xyzquat, but wrong convention.
    poses = np.zeros((in_poses.shape[0], 7), dtype=np.float32)
    poses[:, 0] = in_poses[:,3]
    poses[:, 1:4] = in_poses[:, :3]
  elif pose_format == 'quat_wxyz':
    # poses already in xyzquat, right convention.
    poses = in_poses
  else:
    raise ValueError("Unknown pose format '{}'".format(pose_format))
  return poses

def closeby_or_faraway_indices(poses, max_t_neighbor, max_r_neighbor, min_t_distant, min_r_distant):
  """Constructs two lists of lists declaring a set of poses 'far away' or 'close by' according to some thresholds

  :param poses: poses in xyzquat format (xyzw or wxyz, both work)
  :param max_t_neighbor: maximum distance in meters to declare two poses as neighbors
  :param max_r_neighbor: maximum angle in quaternion distance to declare two poses as neighbors
  :param min_t_distant: minimum distance in meters to declare two poses as distant
  :param min_r_distant: minimum angle in quaternion distance to declare two poses as distant (or'd)
  :return: lists for 'close by' and 'NOT far away' poses
  """
  m = len(poses)
  tenpercent = m // 10
  closeby = []
  not_faraway = []
  for row in range(m):
    if row % tenpercent == 0:
      print("{:2.1%}  {}/{}".format(float(row) / m, row, m))
    dist_t = np.linalg.norm(poses[row, :3] - poses[:, :3], axis=1)
    dist_r = quat_dist(poses[row, 3:], poses[:, 3:])
    row_closeby = np.logical_and(np.less(dist_t, max_t_neighbor), np.less(dist_r, max_r_neighbor))
    row_closeby[row] = False # Don't include the same pose as 'nearby'
    row_not_faraway = np.logical_and(np.less(dist_t, min_t_distant), np.less(dist_r, min_r_distant))
    closeby.append(np.nonzero(row_closeby)[0].astype(np.uint32))
    not_faraway.append(np.nonzero(row_not_faraway)[0].astype(np.uint32))
  return closeby, not_faraway

def create_or_load_neighborhood_lists(list_indices_paths, poses_all_datasets, batching_params, load = True, save = True):
  """
  Returns two lists for each dataset such that:
  
  - closeby_lists[ix_dset][ix_pose] is a list of frames in dataset ix_dset close to pose ix_pose
  - not_faraway_lists[ix_dset][ix_pose] is a list of frames in dataset ix_dset that are 'not far' to pose ix_pose
  
  Poses are considered 'close' when their euclidean distance is below max_t_neighbor,
                              and their quaternion distance is less than max_r_neighbor.
  Poses are considered 'far'  when their euclidean distance is over min_t_distant,
                              or their quaternion distance is over min_r_distant.
  Keep in mind that the returned indices in not_faraway_lists are the 'NOT far' indices.

  :param poses_all_datasets: list (over datasets) of lists of poses
  :param batching_params, containing:
    max_t_neighbor: maximum distance in meters to declare two poses as neighbors
    max_r_neighbor: maximum angle in quaternion distance to declare two poses as neighbors
    min_t_distant: minimum distance in meters to declare two poses as distant
    min_r_distant: minimum angle in quaternion distance to declare two poses as distant (or'd)  :return: closeby_lists, not_faraway_lists
  :param load: If True, it will attempt to load the lists from disk.
  :param save: If True, it will attempt to save the lists to disk (if they're not already there).
  """
  bp = batching_params
  closeby_lists = []
  not_faraway_lists = []
  for path_index, poses_dset in zip(list_indices_paths, poses_all_datasets):
    cacheid = "{:.7f}-{:.7f}-{:.7f}-{:.7f}".format(bp['max_t_neighbor'], bp['max_r_neighbor'],
                                                   bp['min_t_distant'], bp['min_r_distant'])
    path_cache_closeby = path_index.replace('.json', '_closeby-'+ cacheid +'.cachelist')
    path_cache_notfar = path_index.replace('.json', '_notfar-'+ cacheid +'.cachelist')
    if os.path.exists(path_cache_closeby) and os.path.exists(path_cache_notfar) and load:
      print("Loading existing cache files: \n{} \n{}".format(path_cache_closeby, path_cache_notfar))
      with open(path_cache_closeby, 'br') as a, open(path_cache_notfar, 'br') as b:
        closeby = pickle.load(a)
        not_faraway = pickle.load(b)
    else:
      print("Calculating neighborhood for dataset {}".format(path_index))
      closeby, not_faraway = closeby_or_faraway_indices(poses_dset, bp['max_t_neighbor'], bp['max_r_neighbor'],
                                                                    bp['min_t_distant'], bp['min_r_distant'])
      if save:
        print("Saving neighborhood to cache files: \n{} \n{}".format(path_cache_closeby, path_cache_notfar))
        with open(path_cache_closeby, 'bw') as a, open(path_cache_notfar, 'bw') as b:
          pickle.dump(closeby, a)
          pickle.dump(not_faraway, b)
    closeby_lists.append(closeby)
    not_faraway_lists.append(not_faraway)
  return closeby_lists, not_faraway_lists

def minibatch_picker(poses_all_datasets, ratios_list, batching_params, closeby_lists, not_faraway_lists):
  """Sorts and clusters images from a collection of datasets
  Since we're mixing and over/under sampling datasets and dealing with triplets, the concept of epoch is not applicable,
  so we assemble 'stages' of batches_per_stage minibatches at a time (batch_size*batches_per_stage images per stage)


  :param closeby_lists[ix_dset][ix_pose] is a list of frames in dataset ix_dset close to pose ix_pose
  :param not_faraway_lists[ix_dset][ix_pose] is a list of frames in dataset ix_dset that are 'not far' to pose ix_pose
  :param poses_all_datasets: list of lists of image poses in xyzquat format.
  :param ratios_list: list of ratios to use when picking a dataset (a dataset is selected at random for each minibatch)
  :param batching_params, containing:
    batches_per_stage: number of batches that go into a 'stage'
    cluster_size: size of each image cluster
    batch_size: number of images in each minibatch
    max_t_neighbor: maximum distance in meters to declare two poses as neighbors
    max_r_neighbor: maximum angle in quaternion distance to declare two poses as neighbors
    min_t_distant: minimum distance in meters to declare two poses as distant
    min_r_distant: minimum angle in quaternion distance to declare two poses as distant (or'd)
  :return: a list of indices sorted in clusters.
  """
  bp = batching_params

  ratios = np.asarray(ratios_list) / np.sum(ratios_list)
  dset_lengths = np.insert(np.cumsum([len(dset) for dset in poses_all_datasets]), 0, 0)

  all_ixs = []
  for ix_minibatch in range(bp['batches_per_stage']):
    minibatch_ixs = []
    ix_dset = np.random.choice(range(len(poses_all_datasets)), p=ratios)  # pick a dataset
    len_dset = len(poses_all_datasets[ix_dset])
    ix_clustercenter = 0  # init value
    while len(minibatch_ixs) < bp['batch_size']:  # loop over clusters of images
      while True:  # loop if no images are close to the cluster center
        # pick the next cluster center far away from the last one
        not_far_away_indices = not_faraway_lists[ix_dset][ix_clustercenter]
        ix_clustercenter = np.random.randint(len_dset)
        while ix_clustercenter in not_far_away_indices:
          ix_clustercenter = np.random.randint(len_dset)
        closeby_ixs = closeby_lists[ix_dset][ix_clustercenter]
        if len(closeby_ixs) > 0:
          # pick, at most, cluster_sz-1 images close to the cluster center.
          if len(closeby_ixs) > bp['cluster_size'] - 1:
            ixs_b = np.random.choice(closeby_ixs, bp['cluster_size'] - 1, replace=False)
          else:
            ixs_b = closeby_ixs
          minibatch_ixs.append(ix_clustercenter)
          minibatch_ixs.extend(ixs_b)
          break
    all_ixs.extend(minibatch_ixs[:bp['batch_size']] + dset_lengths[ix_dset])
  return (all_ixs)


def input_parser(path, pose, ind, imgproc_func=lambda x: x):
  # read the img from file
  image_string = tf.read_file(path)
  image_decoded = tf.image.decode_png(
    image_string)  # works also for jpg. Don't use decode_image as it returns no shape
  image_processed = imgproc_func(image_decoded)
  return image_processed, pose, ind


def read_dataset(index_path, params = {}):
  """
  :param index_path: Path to a dataset index in .json format
  :param params: Include 'min_t_distant' in this dictionary if using datasets with no pose (only IDs)
  :return: Paths and poses for each image. Poses in (x,y,z,qw,qx,qy,qz) format.
  """
  with open(index_path, 'r') as f:
    d = json.load(f)

  if 'pose_format' not in d.keys():
    print("Outdated .json index. Please add pose_format field. Attempting to convert based on pose dimension")
    pose_format, poses = figure_out_pose_format(d)
  else:
    pose_format, poses = d['pose_format'], d['poses']

  if pose_format == 'id':
    if 'min_t_distant' not in params.keys():
      raise ValueError("You must pass min_t_distant to generate the fake poses for categorical datasets")
    poses = adapt_poses(poses, pose_format, params['min_t_distant'])
  else:
    poses = adapt_poses(poses, pose_format)

  image_dir = os.path.join(os.path.split(index_path)[0], d['im_prefix'])
  full_paths = [os.path.join(image_dir, im_path) for im_path in d['im_paths']]
  return full_paths, poses


def read_multiset(index_path, params = {}):
  """
  Reads a collection of datasets
  :param index_path: Path to the 'index of indices', a json file. It also includes a ratios vector to sub/over sample.
  :param params: Only for the min_t_distant parameter in case of categorical datasets to generate fake poses.
  :return: List of lists of paths, List of lists of poses, dataset ratios and names.
  """
  list_dset_paths = []
  list_dset_poses = []
  list_index_paths = []
  dset_names = []
  with open(index_path, 'r') as f:
    d = json.load(f)

  if 'im_paths' in d.keys():  # just one dataset
    ratios = [1.0]
    full_paths, poses = read_dataset(index_path, params)
    dset_name = os.path.split(index_path)[1]
    return ([full_paths], [poses], [index_path], ratios, [dset_name])
  elif 'ratios' in d.keys():
    ratios = d['ratios']
    for dset_path in d['datasets']:
      full_dset_path = os.path.join(os.path.split(index_path)[0], dset_path)
      list_index_paths.append(full_dset_path)
      full_paths, poses = read_dataset(full_dset_path, params)
      list_dset_paths.append(full_paths)
      list_dset_poses.append(poses)
      dset_names.append(full_dset_path)
  else:
    raise ValueError
  return list_dset_paths, list_dset_poses, list_index_paths, ratios, dset_names

def createdatasets(paths_indices, params, preprocessing_functions, phases):
  """
  Creates queues based on TF's Dataset API. All queues share a single 'get_next' op which returns the next batch.
  The dataset to dequeue from is selected by feeding the corresponding dataset to the shared handle

  :param paths_indices: Paths to N dataset or multiset json index files
  :param params: global params
  :param preprocessing_functions: N preprocessing functions
  :param phases: N strings ('val' or 'train')
  :returns: tuple (next_batch_op, handle, dataset_dicts)
      next_batch_op is the op returning a batch of preprocessed images.
      handle is the shared handle. It must be fed with a dataset specific handle when next_bach_op is run.
      dataset_dicts is a list of dicts containing the iterators, initializators and handles for each dataset (+ more)
  """
  handle = tf.placeholder(tf.string, shape=[], name='handle')
  dataset_dicts = []

  for index_path, phase, func in zip(paths_indices, phases, preprocessing_functions):
    dset_dict = dict()

    # Load the multiset/dataset paths and poses into constant tensors
    list_dset_paths, list_dset_poses, list_index_paths, ratios, dset_names = read_multiset(index_path, params)
    all_paths = [path for paths_dset in list_dset_paths for path in paths_dset]
    all_poses = [pose for poses_dset in list_dset_poses for pose in poses_dset]

    if phase == 'val':
      # Repeat entries to fill the last batch
      real_len = len(all_paths)
      batches_per_dataset = ceildiv(len(all_paths), params['batch_size'])
      eff_length = batches_per_dataset * params['batch_size']
      for i in range(eff_length - len(all_paths)):
        all_poses += [all_poses[-1]]
        all_paths += [all_paths[-1]]

    tensor_paths = tf.constant(all_paths)
    tensor_poses = tf.constant(np.array(all_poses))
    tensor_indexes = tf.constant(np.arange(len(all_paths)))

    # Map the preprocessing function into image loading function
    map_func = functools.partial(input_parser, imgproc_func=func)

    if phase == 'val':
      slices = (tensor_paths, tensor_poses, tensor_indexes)

      threads = params['threads_val']
      buf_siz = params['buffer_val']
      dset_dict['length'] = real_len
    elif phase == 'train':
      # The training queue is regenerated every 'stage' by calculating new indices and feeding them to train_init_ixs
      train_init_ixs = tf.placeholder(tf.int64, shape=[params['batch_size'] * params['batches_per_stage']])
      # The 'sorted' paths and poses are the actual slices that the iterator runs through.
      sorted_train_paths = tf.gather(tensor_paths, train_init_ixs)
      sorted_train_poses = tf.gather(tensor_poses, train_init_ixs)
      # The sorted indices are also extracted as they are needed to perform hard negative mining.
      sorted_train_indexes = tf.gather(tensor_indexes, train_init_ixs)
      slices = (sorted_train_paths, sorted_train_poses, sorted_train_indexes)
      threads = params['threads_train']
      buf_siz = params['buffer_train']
      dset_dict['sorting_ixs'] = train_init_ixs
    else:
      raise ValueError

    dset_dict['image_paths'] = slices[0]
    dset_dict['dataset_source_name'] = dset_names
    dset_dict['paths_indices'] = list_index_paths
    dset_dict['dset_ratios'] = ratios
    dset_dict['list_dset_poses'] = list_dset_poses


    dset = Dataset.from_tensor_slices(slices)
    dset = dset.map(map_func, num_threads=threads, output_buffer_size=buf_siz)
    dset = dset.batch(params['batch_size']) # todo [replace deprecated parameter names of batch() after using TF 1.4]
    dset_dict['dataset'] = dset
    dataset_dicts.append(dset_dict)

  # Shared iterator and get_next op
  dset = dataset_dicts[0]['dataset']
  iterator = tf.contrib.data.Iterator.from_string_handle(handle, dset.output_types, dset.output_shapes)
  next_batch_op = iterator.get_next()

  # Iterators, initializators and handles.
  for dset_dict in dataset_dicts:
    dset = dset_dict['dataset']
    dset_iterator = Iterator.from_structure(dset.output_types, dset.output_shapes)
    dset_dict['iterator'] = dset_iterator
    dset_dict['initializer'] = dset_iterator.make_initializer(dset)

  return (next_batch_op, handle, dataset_dicts)

def main():
  """Test function"""

  datadir = os.path.expanduser('~/test_inputDatasets')
  path_train = os.path.expanduser('~/data/datasets/kitti_odometry/00_left.json')
  path_val = os.path.expanduser('~/data/datasets/kitti_odometry/05_left.json')

  if not os.path.exists(datadir):
    os.mkdir(datadir)

  params = {'cluster_size': 5, 'batch_size': 10, 'buffer_train': 10,
            'threads_train': 2, 'buffer_val': 10, 'threads_val': 2,
            'height': 240, 'width': 320,
            # 'random_crop_max_fraction': 0.1,
            # 'random_brightness_max_delta': 15,
            # 'random_hue_max_delta': 0.05,
            # 'random_saturation_limits': (0.5, 1.5),
            # 'random_contrast_limits': (0.8, 1.1),
            'max_t_neighbor': 2,
            'max_r_neighbor': quat_dist(tr.quaternion_about_axis(20 * np.pi / 180, (0, 0, 1)), (1, 0, 0, 0)),
            'min_t_distant': 50,
            'min_r_distant': quat_dist(tr.quaternion_about_axis(40 * np.pi / 180, (0, 0, 1)), (1, 0, 0, 0)),
            'batches_per_stage': 3
            }
  # Fix the random seeds
  np.random.seed(0)
  random.seed(0)

  sess = tf.Session()
  func_imgproc_train = functools.partial(image_processing.my_training_image_processing, params = params)
  func_imgproc_val = functools.partial(image_processing.simple_resize, params = params)

  next_batch, handle, dataset_dicts = createdatasets((path_train, path_val), params,
                                                     (func_imgproc_train, func_imgproc_val), ('train', 'val'))

  train_dict = dataset_dicts[0]
  training_iterator = train_dict['iterator']
  training_iterator_initializer = train_dict['initializer']
  training_handle = sess.run(training_iterator.string_handle())
  train_dset_ratios = train_dict['dset_ratios']
  train_init_ixs = train_dict['sorting_ixs']
  train_poses = train_dict['list_dset_poses']
  train_paths = train_dict['paths_indices']

  validation_iterator = dataset_dicts[1]['iterator']
  validation_iterator_initializer = dataset_dicts[1]['initializer']
  validation_handle = sess.run(validation_iterator.string_handle())

  stage = 0

  closeby_lists, not_faraway_lists = create_or_load_neighborhood_lists(train_paths, train_poses, params)

  while True: # Run for an infinite number of stages
    if False:
      ixs_train = list(range(train_init_ixs.shape[0]))
      random.shuffle(ixs_train) # quick shuffle
    elif False: # Loop over the one image forever (good to evaluate data augmentations)
      ixs_train = [5 for i in range(train_init_ixs.shape[0])]
    else:
      ixs_train = minibatch_picker(train_poses, train_dset_ratios, params, closeby_lists, not_faraway_lists)

    sess.run(training_iterator_initializer, feed_dict={train_init_ixs: ixs_train})
    step=0
    print('Starting stage', stage)
    while True:
      try:
        images, poses, ind = sess.run(next_batch, feed_dict={handle: training_handle})
        print(images.shape, poses.shape, ind.shape, ind)
        print('step', step)
        for ix in range(images.shape[0]):
          #print(np.min(images[ix]), np.max(images[ix]))
          scipy.misc.imsave("{}/{}_{}.png".format(datadir, step, ix), images[ix].astype(np.uint8))
        save_batch_overview("{}/{}.png".format(datadir, step), images, poses, params)
        step += 1
        
        if step % 5 == 0: # validation
          sess.run(validation_iterator_initializer)
          valstep = 0
          print('Starting validation') #todo check that it still works (batch size x iters = dset len!)
          while True: # run for all the validation set
            try:
              images, poses, ind = sess.run(next_batch, feed_dict={handle: validation_handle})
              valstep += 1
              for ix in range(images.shape[0]):
                scipy.misc.imsave("{}/val{}_{}.jpg".format(datadir, step, ix),
                                  images[ix].astype(np.uint8))
              save_batch_overview("{}/val{}.jpg".format(datadir, step), images, poses, params)
            except tf.errors.OutOfRangeError:
              print('Done with validation!')
              break
            break # break after 1 loop for quicker testing.
        
      except tf.errors.OutOfRangeError:
        break # done with dataset
    exit()
    stage+=1

def save_batch_overview(path, images, poses, params):
    """
    Saves an image to visualize the minibatch.
    """
    import matplotlib
    matplotlib.use('agg')
    import matplotlib.pyplot as plt
    fig = plt.figure(figsize=(15,15))
    N = images.shape[0]
    for i, im in enumerate(images):
        plt.subplot(N+1, N+1, i+2)
        plt.imshow(im.astype(np.uint8))
        plt.axis('off')
        plt.subplot(N+1, N+1, (i+1)*(N+1)+1)
        plt.imshow(im.astype(np.uint8))
        plt.axis('off')

    for row in range(1,N+1):
        for col in range(row+1, N+1):
            i = row*(N+1) + col + 1
            plt.subplot(N+1, N+1, i)

            # Euclidean norm of translation
            dist_t = np.sqrt(np.sum(np.square(poses[row - 1, :3] - poses[col - 1, :3])))

            # Rotation distance
            dist_r = quat_dist(poses[row - 1, 3:], poses[col - 1, 3:])

            if dist_t < params['max_t_neighbor'] and dist_r < params['max_r_neighbor']:
              color = 'g'
            elif dist_t > params['min_t_distant'] or dist_r > params['min_r_distant']:
              color = 'r'
            else:
              color = 'k'

            if(dist_t > 1000):
              plt.text(.5, .5,"{:.2f} km\nr: {:.3f}".format(dist_t/1000, dist_r), ha ='center', color = color)
            else:
              plt.text(.5, .5, "{:.2f} m\nr: {:.3f}".format(dist_t, dist_r), ha='center', color = color)
            plt.xlim(0, 1)
            plt.ylim(0, 1)
            plt.axis('off')

    plt.savefig(path)
    plt.close(fig)

if __name__ == '__main__':
    main()
