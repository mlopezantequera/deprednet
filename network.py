#!/usr/bin/env python3
import os
import sys
import tensorflow as tf
import numpy as np

home = os.path.expanduser('~')
sys.path.append(os.path.join(home, 'work', 'Net_Factory'))
from Net_Factory.importer import names, loads, NET_NAMES, get_network_fn


def networkDefiner(images, params):
  network_vars = dict()

  # Input interface
  images = tf.identity(images, name='input_images')

  ###########       CREATE OR LOAD NETWORK GRAPH       ###########

  # Load one of 'our' networks (not standard from tf.slim)
  if params['net_type'] in names.keys():
    final_endpoint = names[params['net_type']](images, params)
    endpoints = []
    network_vars['embeddings'] = tf.identity(final_endpoint, name='descript_out')

  # Load a standard network from tf.slim
  elif params['net_type'] in NET_NAMES.keys():
    network_fn = get_network_fn(
      params['net_type'],
      num_classes=params['logits_len'],
      weight_decay=0.0,
      is_training=params['is_training'])
    final_endpoint, endpoints = network_fn(images)
    if 'output_layer' in params.keys() and params['output_layer'] in endpoints.keys():
      out = endpoints[params['output_layer']]
    else:
      out = final_endpoint
    print('OUTPUT ', out)
    network_vars['embeddings'] = tf.identity(out, name='descript_out')
  else:
    raise ValueError('Wrong net type')


  ############       OPS FOR LOADING AND/OR INITIALIZING VARIABLES          ############

  # Here we create ops and corresponding feed_dicts that, upon execution, copy the pretrained weights into the graph.

  if params['net_type'] in names.keys():
    # Load pretrained weights for 'our' networks (non-slim format for weights: h5 or npy instead of ckpt)
    if 'absolute_weights_path' in params.keys():
      params['absolute_weights_path'] = os.path.join(os.path.expanduser("~"), params['absolute_weights_path'])
    if 'checkpoint_path' in params.keys():
      params['checkpoint_path'] = os.path.join(os.path.expanduser("~"), params['checkpoint_path'])
    load_ops, load_dicts, pretrained_layers, new_layers = loads[params['net_type']](params)
    # Init op creation and addition to load (ops and dicts)
    init_op = tf.variables_initializer(new_layers)
    load_ops.insert(0, init_op)
    load_dicts.insert(0, {})
    # Addition of load ops to load dicts
    for o in range(len(load_ops) - len(load_dicts)):
      load_dicts.append({})

  elif 'renorm' in params['net_type']: # ======= A hacky solution to 'upgrade' models from batchnorm to batch renorm.
    # Load pretrained weights using the standard tf.slim interface (from ckpt files)
    # We exclude variables listed in the 'exclusions' parameter in the network's json file, as well as 'renorm' variables
    # (Which are then assigned manually)
    if 'checkpoint_path' not in params.keys():
      raise ValueError("'checkpoint_path' is missing in the params file")
    if params['checkpoint_path'][-4:] != 'ckpt':
      raise ValueError("The checkpoint must finish in .ckpt")
    params['checkpoint_path'] = os.path.join(os.path.expanduser("~"), params['checkpoint_path'])
    slim = tf.contrib.slim
    exclusions = params['exclusions'] if 'exclusions' in params.keys() else []
    variables_to_restore, excluded_vars = [], []
    for var in slim.get_model_variables():
      excluded = False
      # We also exclude batch renorm variables. We assume that no model uses batch renorm and that we have to copy batchnorm weights. Will change at some point.
      if 'renorm' in var.name:
        excluded = True
      else:
        for exclusion in exclusions:
          if var.op.name.startswith(exclusion):
            excluded = True
            break
      if excluded:
        excluded_vars.append(var)
      else:
        variables_to_restore.append(var)
    init_op, init_dict = slim.assign_from_checkpoint(params['checkpoint_path'],
                                                     variables_to_restore, ignore_missing_vars=True)

    grouped_vars = {}
    for var in excluded_vars:
      if 'weight' in var.name:
        grouped_vars[var] = 'one'
      elif 'renorm_mean' in var.name:
        grouped_vars[var] = slim.get_variable_full_name(var).replace('renorm_mean', 'moving_mean')
      elif 'renorm_stddev' in var.name:
        grouped_vars[var] = slim.get_variable_full_name(var).replace('renorm_stddev', 'moving_variance')

    feed_dict = {}
    assign_ops = []
    reader = tf.train.NewCheckpointReader(params['checkpoint_path'])
    for var in excluded_vars:
      placeholder_tensor = tf.placeholder(
        dtype=var.dtype.base_dtype,
        shape=var.get_shape(),
        name='placeholder/' + var.op.name)
      assign_ops.append(var.assign(placeholder_tensor))

      if grouped_vars[var] is not 'one':
        ckpt_var = reader.get_tensor(grouped_vars[var])
        if 'variance' in grouped_vars[var]:
          feed_dict[placeholder_tensor] = np.sqrt(ckpt_var.reshape(ckpt_var.shape))
        else:
          feed_dict[placeholder_tensor] = ckpt_var.reshape(ckpt_var.shape)
      else:
        feed_dict[placeholder_tensor] = np.ones(var.get_shape().as_list(), dtype=np.float32)
    init_op2 = tf.group(*assign_ops)
    load_ops = [init_op, init_op2]
    load_dicts = [init_dict, feed_dict]
    new_layers = variables_to_restore + excluded_vars
    pretrained_layers = exclusions
  # ======= End of a hacky solution to 'upgrade' models from batchnorm to batch renorm.

  else:
    # Load pretrained weights using the standard tf.slim interface (from ckpt files)
    # We exclude variables listed in the 'exclusions' parameter in the network's json file.
    if 'checkpoint_path' not in params.keys():
      raise ValueError("'checkpoint_path' is missing in the params file")
    if params['checkpoint_path'][-4:] != 'ckpt':
      raise ValueError("The checkpoint must finish in .ckpt")
    params['checkpoint_path'] = os.path.join(os.path.expanduser("~"), params['checkpoint_path'])
    slim = tf.contrib.slim
    exclusions = params['exclusions'] if 'exclusions' in params.keys() else []
    variables_to_restore = []
    for var in slim.get_model_variables():
      excluded = False
      for exclusion in exclusions:
        if var.op.name.startswith(exclusion):
          excluded = True
          break
      if not excluded:
        variables_to_restore.append(var)
    init_op, init_dict = slim.assign_from_checkpoint(params['checkpoint_path'],
                                                     variables_to_restore, ignore_missing_vars=True)
    load_ops = [init_op]
    load_dicts = [init_dict]
    pretrained_layers = exclusions
    new_layers = variables_to_restore

  print("\n{} pretrained variables, {} new variables\n".format(len(pretrained_layers), len(new_layers)))

  # If there are no pretrained and new layers, they all go to new_layers
  network_vars['pretrained_layers'] = pretrained_layers
  network_vars['new_layers'] = new_layers
  network_vars['endpoints'] = endpoints

  return network_vars, load_ops, load_dicts
