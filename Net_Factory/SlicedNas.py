#!/usr/bin/env python3

import tensorflow as tf
import numpy as np
import Net_Factory.utils.CaffeNet_utils as utils
import Net_Factory.nets_factory as nets_factory
from Net_Factory.utils import loupe


def loadWeights(params):
  slim = tf.contrib.slim
  pretrained_layers = slim.get_model_variables()
  new_layers = []
  for var in tf.global_variables():
    if var not in pretrained_layers:
      new_layers.append(var)
  init_op, init_dict = slim.assign_from_checkpoint(params['checkpoint_path'],
                                                   pretrained_layers, ignore_missing_vars=True)
  load_ops = [init_op]
  load_dicts = [init_dict]
  if len(new_layers) == 0:
    new_layers = pretrained_layers
    pretrained_layers = []
  return load_ops, load_dicts, pretrained_layers, new_layers


def SlicedNas(images, params):
  class_dict = dict()

  class_dict['Normalization'] = params['Normalization']
  class_dict['Activation'] = params['Activation']
  class_dict['NETNAME'] = 'SlicedNas'
  class_dict['VARIABLES'] = params['VARIABLES'] if 'VARIABLES' in params.keys() else 'variables'
  class_dict['UPDATE_OPS_COLLECTION'] = params[
    'UPDATE_OPS_COLLECTION'] if 'UPDATE_OPS_COLLECTION' in params.keys() else 'update_ops'

  if params['Activation'] == 'SELU':
    if params['Rate'] is None or params['Is_training'] is None:
      exit('Rate or is_training missing')
    class_dict['Rate'] = params['Rate']
    class_dict['Is_training'] = params['Is_training']

  network_fn = nets_factory.get_network_fn(
    'nasnet_mobile',
    num_classes=params['nasnet_logits_len'],
    weight_decay=0.0,
    is_training=params['is_training'])
  final_endpoint, endpoints = network_fn(images, final_endpoint=params['output_layer'])

  print("NASNet", params['output_layer'], endpoints[params['output_layer']].get_shape().as_list(),
        np.prod(endpoints[params['output_layer']].get_shape().as_list()[1:]))

  if params['End_type'] == 'GAP':
    out = tf.layers.average_pooling2d(endpoints[params['output_layer']], endpoints[params['output_layer']].get_shape().as_list()[1:3], [1, 1], name="GAP")

  elif params['End_type'] == 'GMP':
    out = tf.layers.max_pooling2d(endpoints[params['output_layer']], endpoints[params['output_layer']].get_shape().as_list()[1:3], [1, 1], name="GMP")

  elif params['End_type'] == 'CNN_GAP':
    layer5 = utils.conv_layer(endpoints[params['output_layer']], "Added_0", 3, endpoints[params['output_layer']].get_shape().as_list()[-1], conv_stride=1,
                              class_dict=class_dict,
                              conv_padding='VALID')
    print("Conv_Added_0", layer5[0].get_shape().as_list(), np.prod(layer5[0].get_shape().as_list()[1:]))

    layer6 = utils.conv_layer(layer5[0], "Added_1", 3, endpoints[params['output_layer']].get_shape().as_list()[-1], conv_stride=1, class_dict=class_dict, conv_padding='VALID')
    print("Conv_Added_1", layer6[0].get_shape().as_list(), np.prod(layer6[0].get_shape().as_list()[1:]))

    out = tf.layers.average_pooling2d(layer6[0], layer6[0].get_shape().as_list()[1:3], [1, 1], name="GAP")

  elif params['End_type'] == 'CNN_GMP':
    layer5 = utils.conv_layer(endpoints[params['output_layer']], "Added_0", 3, endpoints[params['output_layer']].get_shape().as_list()[-1], conv_stride=1,
                              class_dict=class_dict,
                              conv_padding='VALID')
    print("Conv_Added_0", layer5[0].get_shape().as_list(), np.prod(layer5[0].get_shape().as_list()[1:]))

    layer6 = utils.conv_layer(layer5[0], "Added_1", 3, endpoints[params['output_layer']].get_shape().as_list()[-1], conv_stride=1, class_dict=class_dict, conv_padding='VALID')
    print("Conv_Added_1", layer6[0].get_shape().as_list(), np.prod(layer6[0].get_shape().as_list()[1:]))

    out = tf.layers.max_pooling2d(layer6[0], layer6[0].get_shape().as_list()[1:3], [1, 1], name="GMP")

  elif params['End_type'] == 'CNN':
    layer5 = utils.conv_layer(endpoints[params['output_layer']], "Added_0", 3, 256, conv_stride=1,
                              class_dict=class_dict,
                              conv_padding='VALID')
    print("Conv_Added_0", layer5[0].get_shape().as_list(), np.prod(layer5[0].get_shape().as_list()[1:]))

    layer6 = utils.conv_layer(layer5[0], "Added_1", 3, 256, conv_stride=1, class_dict=class_dict, conv_padding='VALID')
    print("Conv_Added_1", layer6[0].get_shape().as_list(), np.prod(layer6[0].get_shape().as_list()[1:]))

    layer7 = utils.conv_layer(layer6[0], "Added_2", 3, 256, conv_stride=1, class_dict=class_dict, conv_padding='VALID')
    print("Conv_Added_2", layer7[0].get_shape().as_list(), np.prod(layer7[0].get_shape().as_list()[1:]))

    layer8 = utils.conv_layer(layer7[0], "Added_3", 3, 128, conv_stride=1, class_dict=class_dict, conv_padding='VALID')
    print("Conv_Added_3", layer8[0].get_shape().as_list(), np.prod(layer8[0].get_shape().as_list()[1:]))

    layer9 = utils.conv_layer(layer8[0], "Added_4", 3, 128, conv_stride=1, class_dict=class_dict,
                              conv_padding='VALID')
    print("Conv_Added_4", layer9[0].get_shape().as_list(), np.prod(layer9[0].get_shape().as_list()[1:]))

    layer10 = utils.conv_layer(layer9[0], "Added_5", 3, 128, conv_stride=1, class_dict=class_dict,
                               conv_padding='VALID')
    print("Conv_Added_5", layer10[0].get_shape().as_list(), np.prod(layer10[0].get_shape().as_list()[1:]))

    out = utils.conv_layer(layer10[0], "Added_6", 3, 128, conv_stride=1, class_dict=class_dict,
                           conv_padding='VALID')
    print("Conv_Added_6", out[0].get_shape().as_list(), np.prod(out[0].get_shape().as_list()[1:]))
    out = out[0]

  elif params['End_type'] == 'VLAD':
    layer5 = utils.conv_layer(endpoints[params['output_layer']], "Added_0", 3,
                            endpoints[params['output_layer']].get_shape().as_list()[-1], conv_stride=1,
                            class_dict=class_dict, conv_padding='VALID')
    print("Conv_Added_0", layer5[0].get_shape().as_list(), np.prod(layer5[0].get_shape().as_list()[1:]))

    layer6 = utils.conv_layer(layer5[0], "Added_1", 3, endpoints[params['output_layer']].get_shape().as_list()[-1],
                          conv_stride=1, class_dict=class_dict, conv_padding='VALID')
    print("Conv_Added_1", layer6[0].get_shape().as_list(), np.prod(layer6[0].get_shape().as_list()[1:]))

    feat_dim = layer6[0].get_shape().as_list()[-1]
    n_feats = int(layer6[0].get_shape().as_list()[1] * layer6[0].get_shape().as_list()[2])
    pooling = loupe.NetVLAD(feature_size=feat_dim, max_samples=n_feats, cluster_size=64,
                            output_dim=256, gating=True, add_batch_norm=False,
                            is_training=params['is_training'])
    reshaped_input = tf.reshape(layer6[0], [-1, feat_dim])  # batch,x,y,ch -> batch*x*y, ch
    out = pooling.forward(reshaped_input)

  else:
    raise ValueError

  out = tf.squeeze(out)

  if params['l2_normalize_embeddings']:
    print("L2-normalizing the embeddings")
    out = tf.nn.l2_normalize(out, dim=1)

  print("Output", out.get_shape().as_list())

  return out
