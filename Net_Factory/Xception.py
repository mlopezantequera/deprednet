import tensorflow as tf
import h5py, os
import Net_Factory.utils.Tensorflow_Xception.load_keras_weights as load_weights_keras
slim = tf.contrib.slim
import Net_Factory.utils.loupe as loupe
from Net_Factory.utils.Tensorflow_Xception.xception import xception, xception_arg_scope

NEEDED_KEYS = ['is_training', 'embedding_len', 'network_topo', 'last_xception_block']

def loadWeights(params):
  if 'absolute_weights_path' not in params.keys():
    ValueError("Needed 'absolute_weights_path' in params file")
  with h5py.File(params['absolute_weights_path'], 'r') as h5f:
    load_ops, pretrained_layers, new_layers = load_weights_keras.get_assign_ops(h5f)
    load_dicts = []
    return load_ops, load_dicts, pretrained_layers, new_layers

def description_net(inputs, params):
  elems = [x for x in NEEDED_KEYS if x not in params.keys()]
  if len(elems) > 0:
    raise ValueError('Some parameters are missing in the params file: ' + str(elems))

  is_training = params['is_training']
  embedding_len = params['embedding_len']
  network_topo = params['network_topo']
  last_xception_block = params['last_xception_block']
  std_dev_fc = 0.001 if 'std_dev_fc' not in params.keys() else params['std_dev_fc']

  with tf.variable_scope('descriptor_net') as sc:

    # Declare the portion of the Xception network that we use up to block last_xception_block
    with tf.contrib.slim.arg_scope(xception_arg_scope()):
      end_points = xception(inputs, is_training=is_training, last_block=last_xception_block)
    last_x_layer = end_points['last_layer']  # Last pretrained layer (corresponding to last_xception_block)

    if True:
      conv_normalizer = None # Don't use batchnorm, use biases
    else:
      conv_normalizer = slim.batch_norm

    # Add new layers to the network that generate the descriptor
    if network_topo == 'xc_conv_fc' or network_topo == 'xc_conv_avgpool':
      with tf.variable_scope('newlayers') as sc:
        with tf.contrib.slim.arg_scope(xception_arg_scope()):
          with slim.arg_scope([slim.batch_norm], is_training=is_training):
            net = slim.conv2d(last_x_layer, embedding_len * 2, [2, 2], padding='valid',
                              biases_initializer=tf.zeros_initializer(),
                              normalizer_fn=conv_normalizer, scope='A_conv2d')
            print(net)
            net = tf.nn.relu(net, name='A_relu')
            net = slim.conv2d(net, embedding_len, [2, 2], padding='valid',
                              biases_initializer=tf.zeros_initializer(),
                              normalizer_fn=conv_normalizer, scope='B_conv2d')
            print(net)
            net = tf.nn.relu(net, name='B_act')
            if network_topo == 'xc_conv_fc':
              net = slim.flatten(net, scope = 'C_flat')
              print(net)
              net = slim.fully_connected(net, embedding_len,
                                                activation_fn=None,
                                                # biases_initializer=None, #comment this line out to enable bias
                                                weights_initializer=tf.truncated_normal_initializer(mean=0, stddev=std_dev_fc),
                                                scope ='descriptor')
              descriptor = tf.nn.l2_normalize(net,1)
            else: # Average pool down to embedding_len elements
              net = slim.avg_pool2d(net, [net.shape[1], net.shape[2]], scope='C_avgpool')
              print(net)
              descriptor = tf.nn.l2_normalize(slim.flatten(net, scope = 'descriptor'), 1)

    elif network_topo == 'xc_vlad':
      feat_dim = int(last_x_layer.shape[1])
      n_feats = int(last_x_layer.shape[2]*last_x_layer.shape[3])
      pooling = loupe.NetVLAD(feature_size=feat_dim, max_samples=n_feats,
                              cluster_size=64, output_dim=embedding_len, gating=True,
                              add_batch_norm=False, is_training=is_training)
      bxyc = tf.transpose(last_x_layer, [0,2,3,1]) #batch,ch,x,y -> batch,x,y,ch
      reshaped_input = tf.reshape(bxyc, [-1, feat_dim]) #batch,x,y,ch -> batch*x*y, ch
      descriptor = pooling.forward(reshaped_input)
    else:
      raise ValueError('unknown network_topo!')

  descriptor_id = tf.identity(descriptor, name='descriptor_out')
  return descriptor

############
