#!/usr/bin/env python3

import tensorflow as tf
import numpy as np
import Net_Factory.utils.CaffeNet_utils as utils

VGG_MEAN = [103.939, 116.779, 123.68]
NEEDED_KEYS = ['embedding_len', 'Normalization', 'Activation']


def loadWeights(params):
  if 'absolute_weights_path' not in params.keys():
    ValueError("Needed 'absolute_weights_path' in params file")
  load_ops, pretrained_layers, new_layers = utils.loadWeights_norm(params)
  load_dicts = []
  return load_ops, load_dicts, pretrained_layers, new_layers


def CaffeNet(input_channel, params, net_name='net'):
  elems = [x for x in NEEDED_KEYS if x not in params.keys()]
  if len(elems) > 0:
    raise ValueError('Some parameters are missing in the params file: ' + str(elems))

  class_dict = dict()
  class_dict['embedding_len'] = params['embedding_len']
  class_dict['VARIABLES'] = params['VARIABLES'] if 'VARIABLES' in params.keys() else 'variables'
  class_dict['UPDATE_OPS_COLLECTION'] = params[
    'UPDATE_OPS_COLLECTION'] if 'UPDATE_OPS_COLLECTION' in params.keys() else 'update_ops'
  class_dict['Normalization'] = params['Normalization']
  class_dict['Activation'] = params['Activation']
  class_dict['NETNAME'] = net_name.replace('/', '')

  if params['Activation'] == 'SELU':
    if params['Rate'] is None or params['Is_training'] is None:
      exit('Rate or is_training missing')
    class_dict['Rate'] = params['Rate']
    class_dict['Is_training'] = params['Is_training']

  if params["net_type"] == 'CaffeOld':
    output = CaffeNetOld(input_channel, class_dict)
  elif params["net_type"] == 'CaffeNew':
    output = CaffeNetNew(input_channel, class_dict)
  else:
    ValueError('Wrong net type')

  return output


def CaffeNetOld(input_channel, class_dict):
  print('input', input_channel.get_shape().as_list())
  norm_gray = tf.to_float(input_channel) - VGG_MEAN
  layer1 = utils.conv_pool_layer(norm_gray, "1", 11, 96, conv_stride=4, pool_size=3, pool_stride=2,
                                 conv_padding='VALID', class_dict=class_dict)
  layer2 = utils.conv_pool_layer(layer1[1], "2", 5, 256, conv_stride=1, pool_size=2, pool_stride=2,
                                 conv_padding='SAME', pool_padding='VALID', group=2,
                                 class_dict=class_dict)
  layer3 = utils.conv_layer(layer2[1], "3", 3, 384, conv_stride=1, class_dict=class_dict)
  layer4 = utils.conv_layer(layer3[0], "4", 3, 384, conv_stride=1, group=2,
                            class_dict=class_dict)
  fc_out = utils.fc_layer(layer4[0], "Fc", class_dict['Embedding_len'], class_dict=class_dict)
  # self.out = tf.nn.softmax(self.fc_out, "Softmax")
  return fc_out


def CaffeNetNew(input_channel, class_dict):
  print('input', input_channel.get_shape().as_list())
  norm_gray = tf.to_float(input_channel) - VGG_MEAN
  layer1 = utils.conv_pool_layer(norm_gray, "1", 11, 96, conv_stride=4, pool_size=3, pool_stride=2,
                                 conv_padding='VALID', class_dict=class_dict)
  layer2 = utils.conv_pool_layer(layer1[1], "2", 5, 256, conv_stride=1, pool_size=2, pool_stride=2,
                                 conv_padding='SAME', pool_padding='VALID', group=2,
                                 class_dict=class_dict)
  layer3 = utils.conv_layer(layer2[1], "3", 3, 384, conv_stride=1, class_dict=class_dict)
  layer4 = utils.conv_layer(layer3[0], "4", 3, 384, conv_stride=1, group=2,
                            class_dict=class_dict)
  layer5 = utils.conv_layer(layer4[0], "5", 1, 192, conv_stride=1, class_dict=class_dict)
  layer6 = utils.conv_layer(layer5[0], "6", 1, 64, conv_stride=1, class_dict=class_dict)
  fc_out = utils.fc_layer(layer6[0], "FC1", class_dict['embedding_len'], class_dict=class_dict)
  # self.out = tf.nn.softmax(self.fc_out, "Softmax")
  return fc_out
