#!/usr/bin/env python3

import tensorflow as tf
from tensorflow.python.framework import ops
from tensorflow.python.framework import tensor_shape
from tensorflow.python.framework import tensor_util
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import random_ops
from tensorflow.python.ops import array_ops
from tensorflow.python.layers import utils
from tensorflow.python.training import moving_averages
import numpy as np
from math import sqrt
import numbers

VGG_MEAN = [103.939, 116.779, 123.68]
CONV_WEIGHT_DECAY = 0
CONV_WEIGHT_STDDEV = 0.1
FC_WEIGHT_STDDEV = 0.001
MOVING_AVERAGE_DECAY = 0.9997
BN_DECAY = MOVING_AVERAGE_DECAY
BN_EPSILON = 0.001
'''
VARIABLES = 'variables'
UPDATE_OPS_COLLECTION = 'update_ops'
'''


def selu(x, name="selu"):
  """ When using SELUs you have to keep the following in mind:
  # (1) scale inputs to zero mean and unit variance
  # (2) use SELUs
  # (3) initialize weights with stddev sqrt(1/n)
  # (4) use SELU dropout
  """
  with ops.name_scope(name):
    alpha = 1.6732632423543772848170429916717
    scale = 1.0507009873554804934193349852946
    return scale * tf.where(x >= 0.0, x, alpha * tf.nn.elu(x))


def act(x, class_dict):
  if class_dict['Activation'] == 'RELU':
    x = tf.nn.relu(x)
  elif class_dict['Activation'] == 'ELU':
    x = tf.nn.elu(x)
  elif class_dict['Activation'] == 'SELU':
    x = selu(x)
  else:
    exit('\nActivation function not correct\n')
  return x


def conv_conv2_layer(bottom, name, conv_size, depth, class_dict, conv_stride=2, group1=1, group2=1, conv_size2=None,
                     last=False, conv_padding1='SAME', conv_padding2='SAME', verbose=False):
  if conv_size2 is None:
    conv_size2 = conv_size
  if group1 != 1 or group2 != 1:
    if verbose:
      print(class_dict['NETNAME'] + '/' + "Layer " + name + " has groups!!!! This is a Caffe charasteristic")

  with tf.variable_scope(class_dict['NETNAME'] + '/' + 'Layer_' + name):
    first = conv_unit(bottom, 'conv_' + name + '_1', [conv_size, conv_size, conv_stride, depth],
                      group=group1, padding=conv_padding1, class_dict=class_dict)
    if verbose:
      print('conv_' + name + '_1', first.get_shape().as_list())
    second = conv_unit(first, 'conv_' + name + '_2', [conv_size2, conv_size2, conv_stride, depth],
                       last=last, group=group2, padding=conv_padding2, class_dict=class_dict)
    if verbose:
      print('conv_' + name + '_2', second.get_shape().as_list())
  return [first, second]


def conv_pool_layer(bottom, name, conv_size, depth, class_dict, conv_stride=2, pool_size=2, pool_stride=2,
                    conv_padding='SAME', pool_padding='SAME', group=1, verbose=False):
  if group != 1:
    if verbose:
      print(class_dict['NETNAME'] + '/' + "Layer " + name + " has groups!!!! This is a Caffe charasteristic")

  with tf.variable_scope(class_dict['NETNAME'] + '/' + 'Layer_' + name):
    first = conv_unit(bottom, 'conv_' + name, [conv_size, conv_size, conv_stride, depth], group=group,
                      padding=conv_padding, class_dict=class_dict)
    if verbose:
      print('conv_' + name, first.get_shape().as_list())
    pool1 = max_pool(first, 'pool_' + name, pool_size, pool_stride, padding=pool_padding)
    if verbose:
      print('pool_' + name, pool1.get_shape().as_list())
  return [first, pool1]


def up_conv_layer(bottom, name, conv_size, depth, class_dict, conv_stride=1, last=False, upsample_shape=None,
                  conv_padding='SAME', group=1, verbose=False):
  if group != 1:
    if verbose:
      print(class_dict['NETNAME'] + '/' + "Layer " + name + " has groups!!!! This is a Caffe charasteristic")

  with tf.variable_scope(class_dict['NETNAME'] + '/' + 'Layer_' + name):
    upsample = resize_conv(bottom, 'upsample_' + name, shape=upsample_shape)
    if verbose:
      print('upsample_' + name, upsample.get_shape().as_list())
    out = conv_unit(upsample, 'conv_' + name, [conv_size, conv_size, conv_stride, depth], last=last,
                    group=group, padding=conv_padding, class_dict=class_dict)
    if verbose:
      print('conv_' + name, out.get_shape().as_list())
  return [upsample, out]


def conv_layer(bottom, name, conv_size, depth, class_dict, conv_stride=1, last=False, conv_padding='SAME', group=1,
               verbose=False):
  if group != 1:
    if verbose:
      print(class_dict['NETNAME'] + '/' + "Layer " + name + " has groups!!!! This is a Caffe charasteristic")
  with tf.variable_scope(class_dict['NETNAME'] + '/Layer_' + name):
    first = conv_unit(bottom, 'conv_' + name, [conv_size, conv_size, conv_stride, depth], last=last,
                      group=group, padding=conv_padding, class_dict=class_dict)
    if verbose:
      print('conv_' + name, first.get_shape().as_list())
  return [first]


def fc_layer(bottom, name, out_size, class_dict, bias=True, last=False):
  with tf.variable_scope(class_dict['NETNAME'] + '/' + name):
    with tf.variable_scope('Reshape'):
      shape = bottom.get_shape().as_list()
      dim = 1
      for d in shape[1:]:
        dim *= d
      x = tf.reshape(bottom, [-1, dim])
    initializer = tf.truncated_normal_initializer(0.0, FC_WEIGHT_STDDEV)
    weights = _get_variable(name + '_w', [dim, out_size], varis=class_dict['VARIABLES'], initializer=initializer)

    if bias:
      biases = _get_variable(name + '_b', [out_size], varis=class_dict['VARIABLES'],
                             initializer=tf.zeros_initializer())
      out = tf.nn.xw_plus_b(x, weights, biases, name='FC_' + name)
    else:
      out = tf.matmul(x, weights)
    if class_dict['Normalization'] == 'BN':
      out = bn(out, update_ops=class_dict['UPDATE_OPS_COLLECTION'], varis=class_dict['VARIABLES'])
    elif class_dict['Normalization'] == 'LRN':
      out = tf.nn.lrn(tf.matmul(x, weights, name='FC_' + name))

    if not last:
      out = act(out, class_dict)

    print('fc_' + name, out.get_shape().as_list())
  return out


def conv_unit(bottom, name, shape, padding, group, class_dict, last=False, bias=True):
  with tf.variable_scope(name):
    con = conv(bottom, shape[0], shape[2], shape[3], name, group, padding=padding, class_dict=class_dict)

    if bias:
      bias = _get_variable(name + '_b', shape[-1:], varis=class_dict['VARIABLES'],
                           initializer=tf.zeros_initializer())
      out = tf.nn.bias_add(con, bias)
    else:
      out = con
    if class_dict['Normalization'] == 'BN':
      out = bn(out, update_ops=class_dict['UPDATE_OPS_COLLECTION'], varis=class_dict['VARIABLES'])
    elif class_dict['Normalization'] == 'LRN':
      out = tf.nn.lrn(out)

    if not last:
      out = act(out, class_dict)
      if class_dict['Activation'] == selu:
        out = dropout_selu(out, class_dict['Rate'], training=class_dict['Is_training'])
  return out


def resize_conv(x, name, shape=None, upsample_rate=4):
  with tf.variable_scope(name):
    x_shape = x.get_shape().as_list()
    if shape is None:
      height = x_shape[1] * upsample_rate
      width = x_shape[2] * upsample_rate
    else:
      height = shape[0]
      width = shape[1]
    resized = tf.image.resize_bilinear(x, [height, width])
  return resized


def deconv_layer(bottom, name, shape, class_dict, last=False):
  with tf.variable_scope(name):
    con = deconv(bottom, shape[0], shape[2], shape[3], class_dict=class_dict)

    bias = _get_variable(name + '_b', shape[-1:], varis=class_dict['VARIABLES'],
                         initializer=tf.zeros_initializer())
    out = tf.nn.bias_add(con, bias)

    if not last:
      out = act(out, class_dict)
  return out


def conv(x, ksize, stride, filters_out, name, group, padding, class_dict):
  convolve = lambda i, k: tf.nn.conv2d(i, k, [1, stride, stride, 1], padding=padding)

  filters_in = x.get_shape()[-1]
  shape = [ksize, ksize, int(filters_in) / group, filters_out]

  # Determine number of input features from shape
  f_in = np.prod(shape[:-1]) if len(shape) == 4 else shape[0]

  # Calculate sdev for initialization according to activation function
  if class_dict['Activation'] == 'SELU':
    sdev = sqrt(1 / f_in)
  elif class_dict['Activation'] == 'RELU':
    sdev = sqrt(2 / f_in)
  elif class_dict['Activation'] == 'ELU':
    sdev = sqrt(1.5505188080679277 / f_in)
  else:
    sdev = CONV_WEIGHT_STDDEV

  initializer = tf.truncated_normal_initializer(stddev=sdev)
  weights = _get_variable(name + '_w', varis=class_dict['VARIABLES'],
                          shape=shape,
                          dtype=tf.float32,
                          initializer=initializer,
                          weight_decay=CONV_WEIGHT_DECAY)
  if group == 1:
    # This is the common-case. Convolve the input without any further complications.
    output = convolve(x, weights)
  else:
    with tf.variable_scope('Group_op'):
      # Split the input into groups and then convolve each of them independently
      input_groups = tf.split(x, group, 3)
      kernel_groups = tf.split(weights, group, 3)
      output_groups = [convolve(i, k) for i, k in zip(input_groups, kernel_groups)]
      # Concatenate the groups
      output = tf.concat(output_groups, 3)

  return output


def max_pool(bottom, name, pool_size=2, pool_stride=2, padding='SAME'):
  return tf.nn.max_pool(bottom, ksize=[1, pool_size, pool_size, 1], strides=[1, pool_stride, pool_stride, 1],
                        padding=padding, name=name)


def deconv(x, ksize, stride, filters_out, class_dict):
  # print(x.get_shape().as_list())
  batch_size = x.get_shape()[0]
  out_h = x.get_shape()[1] * stride
  out_w = x.get_shape()[2] * stride
  out_shape = tf.stack([batch_size, out_h, out_w, filters_out])
  # print([batch_size, out_h, out_w, filters_out])

  shape = [ksize, ksize, filters_out, x.get_shape()[3]]
  # print(shape)
  # initializer = tf.contrib.layers.xavier_initializer(dtype=tf.float32)
  initializer = tf.truncated_normal_initializer(stddev=CONV_WEIGHT_STDDEV)
  weights = _get_variable('weights', varis=class_dict['VARIABLES'], shape=shape, dtype=tf.float32,
                          initializer=initializer,
                          weight_decay=CONV_WEIGHT_DECAY)
  return tf.nn.conv2d_transpose(x, weights, out_shape, [1, stride, stride, 1],
                                padding='SAME')


def _get_variable(name,
                  shape,
                  initializer,
                  varis='variables',
                  weight_decay=0.0,
                  dtype=tf.float32,
                  trainable=True):
  """A little wrapper around tf.get_variable to do weight decay and add to resnet collection"""
  if weight_decay > 0:
    regularizer = tf.contrib.layers.l2_regularizer(weight_decay)
  else:
    regularizer = None

  collections = [tf.GraphKeys.GLOBAL_VARIABLES, varis]
  return tf.get_variable(name,
                         shape=shape,
                         initializer=initializer,
                         dtype=dtype,
                         regularizer=regularizer,
                         collections=collections,
                         trainable=trainable)


def bn(x, update_ops='update_ops', varis='variables'):
  x_shape = x.get_shape()
  params_shape = x_shape[-1:]

  axis = list(range(len(x_shape) - 1))

  beta = _get_variable('beta',
                       params_shape, varis=varis,
                       initializer=tf.zeros_initializer())
  gamma = _get_variable('gamma',
                        params_shape, varis=varis,
                        initializer=tf.ones_initializer())

  moving_mean = _get_variable('moving_mean',
                              params_shape, varis=varis,
                              initializer=tf.zeros_initializer(),
                              trainable=False)
  moving_variance = _get_variable('moving_variance',
                                  params_shape, varis=varis,
                                  initializer=tf.ones_initializer(),
                                  trainable=False)

  # These ops will only be preformed when training.
  mean, variance = tf.nn.moments(x, axis)
  update_moving_mean = moving_averages.assign_moving_average(moving_mean,
                                                             mean, BN_DECAY)
  update_moving_variance = moving_averages.assign_moving_average(
    moving_variance, variance, BN_DECAY)
  tf.add_to_collection(update_ops, update_moving_mean)
  tf.add_to_collection(update_ops, update_moving_variance)

  '''mean, variance = control_flow_ops.cond(
      c['is_training'], lambda: (mean, variance),
      lambda: (moving_mean, moving_variance))'''

  x = tf.nn.batch_normalization(x, moving_mean, moving_variance, beta, gamma, BN_EPSILON)

  return x


def dropout_selu(x, rate, alpha=-1.7580993408473766, fixedPointMean=0.0, fixedPointVar=1.0,
                 noise_shape=None, seed=None, name=None, training=False):
  """Dropout to a value with rescaling."""

  def dropout_selu_impl(x, rate, alpha, noise_shape, seed):
    keep_prob = 1.0 - rate
    x = ops.convert_to_tensor(x, name="x")
    if isinstance(keep_prob, numbers.Real) and not 0 < keep_prob <= 1:
      raise ValueError("keep_prob must be a scalar tensor or a float in the "
                       "range (0, 1], got %g" % keep_prob)
    keep_prob = ops.convert_to_tensor(keep_prob, dtype=x.dtype, name="keep_prob")
    keep_prob.get_shape().assert_is_compatible_with(tensor_shape.scalar())

    alpha = ops.convert_to_tensor(alpha, dtype=x.dtype, name="alpha")
    alpha.get_shape().assert_is_compatible_with(tensor_shape.scalar())

    if tensor_util.constant_value(keep_prob) == 1:
      return x

    noise_shape = noise_shape if noise_shape is not None else array_ops.shape(x)
    random_tensor = keep_prob
    random_tensor += random_ops.random_uniform(noise_shape, seed=seed, dtype=x.dtype)
    binary_tensor = math_ops.floor(random_tensor)
    ret = x * binary_tensor + alpha * (1 - binary_tensor)

    a = math_ops.sqrt(fixedPointVar / (
      keep_prob * ((1 - keep_prob) * math_ops.pow(alpha - fixedPointMean, 2) + fixedPointVar)))

    b = fixedPointMean - a * (keep_prob * fixedPointMean + (1 - keep_prob) * alpha)
    ret = a * ret + b
    ret.set_shape(x.get_shape())
    return ret

  with ops.name_scope(name, "dropout", [x]):
    return utils.smart_cond(training,
                            lambda: dropout_selu_impl(x, rate, alpha, noise_shape, seed),
                            lambda: array_ops.identity(x))


def loadWeights_norm(params, type=''):
  fileVars = np.load(params['absolute_weights_path'])
  layerVars = tf.global_variables()
  pretr_layers = [x for x in fileVars.files if x not in params['new_layers']]

  load_ops = []
  pretrainedLayers = []
  for pLay in pretr_layers:
    assignVar = [x for x in layerVars if pLay + ':' in x.name]
    pretrainedLayers = pretrainedLayers + assignVar
    for num in range(len(assignVar)):
      if 'Fc_w' in pLay:
        shap = assignVar[num].get_shape().as_list()
        weight = np.reshape(fileVars[pLay], [shap[0], shap[1]])
        load_ops.append(assignVar[num].assign(weight))
      else:
        load_ops.append(assignVar[num].assign(fileVars[pLay]))

  newLayers = [x for x in layerVars if x not in pretrainedLayers]
  for pLay in params['new_layers']:
    assignVar = [x for x in layerVars if pLay in x.name]
    newLayers = newLayers + assignVar

  # print(new_layers,newLayers)
  # print(pretr_layers,pretrainedLayers)
  # print(load_ops)

  if params['net_type'] == 'CaffeNew':
    pretrainedLayers = pretrainedLayers
  elif params['net_type'] == 'CaffeOld':
    pretrainedLayers = []
  else:
    raise ValueError('Wrong net type when loading weights')

  return load_ops, pretrainedLayers, newLayers
