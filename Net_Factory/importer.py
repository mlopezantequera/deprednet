from __future__ import absolute_import

import Net_Factory.CaffeNet as CaffeNet
import Net_Factory.Xception as Xception
import Net_Factory.nets.nets_factory as nets_factory
import Net_Factory.SlicedNas as SlicedNas
import Net_Factory.Renorm_nasnet as renorm_nasnet

names = {'CaffeOld': CaffeNet.CaffeNet,
         'CaffeNew': CaffeNet.CaffeNet,
         'Xception': Xception.description_net,
         'SlicedNas': SlicedNas.SlicedNas}

loads = {'CaffeOld': CaffeNet.loadWeights,
         'CaffeNew': CaffeNet.loadWeights,
         'Xception': Xception.loadWeights,
         'SlicedNas': SlicedNas.loadWeights}

networks_map = nets_factory.networks_map
arg_scopes_map = nets_factory.arg_scopes_map
get_network_fn = nets_factory.get_network_fn

networks_map['nasnet_mobile_renorm'] = nets_factory.networks_map['nasnet_mobile']
arg_scopes_map['nasnet_mobile_renorm'] = renorm_nasnet.nasnet_mobile_renorm_arg_scope

NET_NAMES = {**names, **networks_map}
