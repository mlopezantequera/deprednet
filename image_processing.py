# Preprocessing for training / validation images
import tensorflow as tf

# The functions within this file are independent of the input pipeline,
# They take image tensors and output image tensors.

def simple_resize(im, params = {}):
  im = tf.image.resize_images(im, [params['height'], params['width']])
  return im

def my_training_image_processing(im, params = {}):
  if 'random_crop_max_fraction' in params.keys():
    shape = tf.shape(im)
    max_px = params['random_crop_max_fraction'] * tf.to_float(tf.reduce_min(shape[:2], axis = 0))
    x = tf.to_int32(tf.random_uniform([4], 0, max_px)) # 4 edges to crop off
    im = im[x[2]:tf.subtract(shape[0], x[0]), x[3]:tf.subtract(shape[1], x[1]), :]

  im = tf.image.resize_images(im, [params['height'], params['width']])

  if 'random_brightness_max_delta' in params.keys():
    im = tf.image.random_brightness(im, params['random_brightness_max_delta'])
    im = tf.clip_by_value(im, 0, 255)
  if 'random_hue_max_delta' in params.keys():
    im = tf.image.random_hue(im, params['random_hue_max_delta'])
  if 'random_saturation_limits' in params.keys():
    im = tf.image.random_saturation(im, params['random_saturation_limits'][0], params['random_saturation_limits'][1])
  if 'random_contrast_limits' in params.keys():
    im = tf.image.random_contrast(im, params['random_contrast_limits'][0], params['random_contrast_limits'][1])
    im = tf.clip_by_value(im, 0, 255)
  return im