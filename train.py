#!/usr/bin/env python3

#SBATCH --job-name=depred
#SBATCH --time=24:00:00
#SBATCH --mem=64000
#SBATCH --nodes=1

import numpy as np
import time
import os
import json
import h5py
import tensorflow as tf
from scipy.spatial.distance import pdist, squareform, cdist
import sklearn
if int(sklearn.__version__.split('.')[1]) < 19:
  raise ImportError("scikit-learn v 0.19 or greater required!")
from sklearn.metrics.ranking import precision_recall_curve, average_precision_score
from random import randint
import gc

home = os.path.expanduser('~')

import functools

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

import transformations as tr
from descriptor_loss import *
from network import networkDefiner
import inputDatasets
import image_processing
import extra
import Preprocessing_Factory.preprocessing_factory as preprocessor


flags = tf.app.flags
FLAGS = flags.FLAGS

# Optimizer parameters
flags.DEFINE_float('learning_rate', 0.000001, 'Learning rate for pretrained layers')
flags.DEFINE_float('pretrained_learning_rate', 0, 'Learning rate for pretrained layers')
flags.DEFINE_string('lr_policy', 'constant', 'Learning rate policy. \'constant \' or \'exponential\'')
flags.DEFINE_float('exp_base', 2.0, 'base of the exponent if lr_policy == exponential')
flags.DEFINE_integer('slow_warmup', 0, 'The LR will linearly increase from 0 during the first slow_warmup steps' +
                     ', this is independent of lr_policy')
# Other stuff
flags.DEFINE_bool('save_weight_histograms', False, 'Enable this to save weight histograms')
flags.DEFINE_bool('save_sample_triplets', False, 'Save training triplets for debugging')
flags.DEFINE_integer('max_steps', 1000000, 'Number of steps to run trainer.')
flags.DEFINE_integer('validation_steps', 250, 'Number of steps to validate')
flags.DEFINE_float('max_hours', -1.0, 'Stop training after max_hours hours')

# Loss parameters
flags.DEFINE_string('loss_type', 'triplet', 'kernel / contrastive / triplet')
flags.DEFINE_float('sigma_t', 15.0, 'Translation sigma for the pose kernel, in meters.')
flags.DEFINE_float('sigma_r', 30, 'Rotation sigma for the pose kernel, in degrees.')
flags.DEFINE_float('beta', .5, 'margin for contrastive and triplet losses')
flags.DEFINE_float('th_t', 15.0,
                   'Maximum distance(m) to consider an image pair a match for contrastive and triplet losses.')
flags.DEFINE_float('th_r', 20.0,
                   'Maximum angle(deg) to consider an image pair a match for contrastive and triplet losses.')
flags.DEFINE_float('min_t_distant', -1,
                   'Minimum distance(m) to consider an image pair \'distant\' for contrastive and triplet losses. Defaults to 10*th_t')
flags.DEFINE_float('min_r_distant', -1,
                   'Minimum angle(deg) to consider an image pair \'distant\' contrastive and triplet losses. Defaults to  3*th_r')

# Validation parameters
flags.DEFINE_float('tol_t', 15.0, 'Maximum distance(m) to consider an image pair a match for VALIDATION')
flags.DEFINE_float('tol_r', 20.0, 'Maximum angle(deg) to consider an image pair a match for VALIDATION')
flags.DEFINE_bool('do_initial_eval', True, 'Perform evaluation on step 0')

# Batching parameters
flags.DEFINE_integer('cluster_size', 1, 'Cluster size.'
                                        'A minibatch is formed by groups of (cluster_size) images that are taken from similar viewpoints')
flags.DEFINE_integer('batch_size', 3, 'Batch size. Should be a multiple of cluster_size')
flags.DEFINE_integer('batches_per_stage', 10000, 'Number of batches forming a \'stage\'.'
                                                 'A stage is a randomly selected mix of images from the available datasets.'
                                                 'After a stage is over, a new set is selected (randomly or (to-do) using hard negative mining')

# Hard Negative Params
flags.DEFINE_integer('hn_cache_size', 1000, 'Size of the cluster for the hard negative mining')
flags.DEFINE_integer('prev_cache_size', 1000, 'Size of the cache of the previous stage')
flags.DEFINE_bool('do_hn_mining', False, 'Perform Hard Negative Mining')

# Paths to datasets, config files, etc
datadir = os.path.join(home, 'data')
flags.DEFINE_string('log_dir', os.path.join(datadir, 'deprednet', 'debug_data'), 'Directory to put the logs.')
flags.DEFINE_string('snapshot_dir', os.path.join(datadir, 'deprednet', 'debug_data'), 'Directory to put the snapshots.')
flags.DEFINE_string('net_file_path', './net_params_caffenet.json', 'File that contains the net params.')
flags.DEFINE_string('training_set', os.path.join(datadir, "datasets/multi_set_1.json"),
                    'Dataset to use for training (.json index file)')
flags.DEFINE_string('validation_set', os.path.join(datadir, "datasets/kitti_odometry/05_left.json"),
                    'Dataset to use for validation (.json index file)')
flags.DEFINE_string('validation_set_2', "",
                    'Second dataset to use 2-dataset validation (.json index file)')

# Other stuff
flags.DEFINE_bool('data_augmentation', True, 'Perform data augmentation. Hardcoded parameters for now')
flags.DEFINE_bool('fix_random_seed', False, 'Sets the random seeds to 0. (todo: some randomized TF ops still random)')

def deg_to_quatdist(d):
  return inputDatasets.quat_dist(tr.quaternion_about_axis(d * np.pi / 180, (0, 0, 1)), (1, 0, 0, 0))

def run_training():
  if FLAGS.fix_random_seed:
    np.random.seed(0)
    tf.set_random_seed(0)

  os.makedirs(FLAGS.snapshot_dir, exist_ok=True)
  os.makedirs(FLAGS.log_dir, exist_ok=True)
  if FLAGS.save_sample_triplets:
    os.makedirs(FLAGS.log_dir + '/triplets/nonz', exist_ok=True)
    os.makedirs(FLAGS.log_dir + '/triplets/zero', exist_ok=True)
    # Show the indexes triplets
    # os.makedirs(FLAGS.log_dir + '/triplets/indexes', exist_ok=True)

  with open(FLAGS.snapshot_dir + '/flags.json', 'w') as f:
    json.dump(tf.flags.FLAGS.__flags, f)

  with open(FLAGS.net_file_path, 'r+') as f:
    net_params = json.load(f)
  print(net_params)

  with open(FLAGS.snapshot_dir + '/net_params.json', 'w') as f:
    json.dump({k: v for k, v in net_params.items() if k is not 'is_training'}, f)

  config = tf.ConfigProto(log_device_placement=False)
  config.gpu_options.allow_growth = True
  sess = tf.Session(config=config)

  height, width = net_params['image_height'], net_params['image_width']
  is_training = tf.placeholder(tf.bool, shape=[], name='is_training')

  if FLAGS.do_hn_mining:
    hn_indexes = np.zeros([FLAGS.hn_cache_size, 3], dtype=np.int32)
    hn_losses = np.zeros([FLAGS.hn_cache_size], dtype=np.float32)

  # Sanity check some parameters
  if FLAGS.min_t_distant == -1:
    FLAGS.min_t_distant = 10 * FLAGS.th_t
  if FLAGS.min_r_distant == -1:
    FLAGS.min_r_distant = 3 * FLAGS.th_r
  assert FLAGS.min_t_distant >= FLAGS.th_t, "th_t should be less than or equal to min_t_distant!"
  assert FLAGS.min_r_distant >= FLAGS.th_r, "th_r should be less than or equal to min_r_distant!"

  # Translate degrees into the equivalent quaternion norm values.
  tol_quat = deg_to_quatdist(FLAGS.tol_r)
  th_quat = deg_to_quatdist(FLAGS.th_r)
  min_distant_quat = deg_to_quatdist(FLAGS.min_r_distant)
  sigma_r_quat = deg_to_quatdist(FLAGS.sigma_r)

  # Setup input pipeline
  # todo move this to an external config file
  '''if FLAGS.data_augmentation:
    imgproc_params = {'height': height, 'width': width, 'random_crop_max_fraction': .1,
                      'random_brightness_max_delta': 15, 'random_hue_max_delta': 0.05,
                      'random_saturation_limits': (0.5, 1.5), 'random_contrast_limits': (0.8, 1.1)}
  else:
    imgproc_params = {'height': height, 'width': width}'''

  batching_params = {'cluster_size': FLAGS.cluster_size, 'batch_size': FLAGS.batch_size,
                     'buffer_train': 500, 'threads_train': 8, 'buffer_val': 500, 'threads_val': 8,
                     'batches_per_stage': FLAGS.batches_per_stage,
                     'max_t_neighbor': FLAGS.th_t, 'max_r_neighbor': FLAGS.th_r,
                     'min_t_distant': FLAGS.min_t_distant, 'min_r_distant': FLAGS.min_r_distant}

  image_preprocessing_train = preprocessor.get_preprocessing(net_params['net_type'], is_training=True)
  image_preprocessing_eval = preprocessor.get_preprocessing(net_params['net_type'], is_training=False)

  if 'preprocessing_params' in net_params.keys():
    func_imgproc_train = functools.partial(image_preprocessing_train, output_height=height, output_width=width, params=net_params['preprocessing_params'])
    func_imgproc_val = functools.partial(image_preprocessing_eval, output_height=height, output_width=width, params=net_params['preprocessing_params'])
  else:
    func_imgproc_train = functools.partial(image_preprocessing_train, output_height=height, output_width=width)
    func_imgproc_val = functools.partial(image_preprocessing_eval, output_height=height, output_width=width)

  paths_indices = [FLAGS.training_set, FLAGS.validation_set]
  preprocessing_functions = [func_imgproc_train, func_imgproc_val]
  phases = ['train', 'val']
  if FLAGS.validation_set_2 != "":
    paths_indices.append(FLAGS.validation_set_2)
    preprocessing_functions.append(func_imgproc_val)
    phases.append('val')
  
  next_batch, handle, dataset_dicts = inputDatasets.createdatasets(paths_indices, batching_params, preprocessing_functions, phases)

  train_dict = dataset_dicts[0]
  training_iterator = train_dict['iterator']
  training_iterator_initializer = train_dict['initializer']
  training_handle = sess.run(training_iterator.string_handle())
  train_dset_names = train_dict['dataset_source_name']
  train_dset_ratios = train_dict['dset_ratios']
  train_init_ixs = train_dict['sorting_ixs']
  train_poses = train_dict['list_dset_poses']
  all_train_paths = train_dict['image_paths']

  validation_iterator = dataset_dicts[1]['iterator']
  validation_iterator_initializer = dataset_dicts[1]['initializer']
  validation_handle = sess.run(validation_iterator.string_handle())
  eff_len_val = dataset_dicts[1]['length']
  eval_poses = np.zeros((eff_len_val, 7), dtype=np.float32) # x,y,z,qw,qx,qy,qz

  if FLAGS.validation_set_2 != "":
    val2_iterator = dataset_dicts[2]['iterator']
    val2_iterator_initializer = dataset_dicts[2]['initializer']
    validation_2_handle = sess.run(val2_iterator.string_handle())
    eff_len_val2 = dataset_dicts[2]['length']
    eval_poses_2 = np.zeros((eff_len_val2, 7), dtype=np.float32) # x,y,z,qw,qx,qy,qz

  batch_poses = tf.reshape(next_batch[1], (FLAGS.batch_size, 7))
  batch_images = tf.reshape(next_batch[0], (FLAGS.batch_size, height, width, 3))
  batch_indexes = tf.reshape(next_batch[2], (FLAGS.batch_size,))

  print('\nbatch_images.shape: ', batch_images.shape)
  print('batch_poses.shape: ', batch_poses.shape)
  print('batch_indexes.shape: ', batch_indexes.shape, '\n')

  train_lengths = np.cumsum([len(x) for x in train_poses])

  # left-right flip on the batch during training
  if FLAGS.data_augmentation:
    mirror_cond = tf.less(tf.random_uniform([], 0, 1.0), .5)
    randomly_flipped_batch = tf.where(mirror_cond, tf.reverse(batch_images, [2]), batch_images)
    batch_images = tf.where(is_training, randomly_flipped_batch, batch_images)

  net_params['is_training'] = True
  network_vars, load_ops, load_dicts = networkDefiner(batch_images, net_params)

  embeddings = network_vars['embeddings']
  pretrained_layers = network_vars['pretrained_layers']
  new_layers = network_vars['new_layers']

  eval_embeddings = np.zeros((eff_len_val, embeddings.get_shape()[-1]), dtype=np.float32)
  if FLAGS.validation_set_2 != "":
    eval_embeddings_2 = np.zeros((eff_len_val2, embeddings.get_shape()[-1]), dtype=np.float32)

  if FLAGS.loss_type == 'kernel':
    d_loss, d_dist, kernel_a, kernel_pose = kernel_loss(embeddings, batch_poses, FLAGS.sigma_t, sigma_r_quat)
  elif FLAGS.loss_type == 'contrastive':
    d_loss = contrastive_loss(embeddings, batch_poses, FLAGS.th_t, th_quat, FLAGS.beta)
  elif FLAGS.loss_type == 'triplet':
    d_loss, element_loss, valid_triplets, nonzero_triplets, triplet_indexes = triplet_loss(embeddings, batch_poses,
                                                                                           batch_indexes, FLAGS.th_t,
                                                                                           th_quat, FLAGS.min_t_distant,
                                                                                           min_distant_quat, FLAGS.beta)
  else:
    raise ValueError('unknown loss type ' + FLAGS.loss_type)

  # Show the indexes triplets
  # im_paths = tf.gather(all_train_paths, batch_indexes)
  loss = d_loss
  tf.summary.scalar(d_loss.op.name, d_loss)

  for op, dict in zip(load_ops, load_dicts):
    sess.run(op, feed_dict=dict)

  global_step = tf.get_variable(initializer=tf.zeros_initializer, name='global_step', shape=[], trainable=False)
  lr = tf.get_variable(initializer=tf.constant(FLAGS.learning_rate), name='lr', trainable=False)
  tf.summary.scalar('learning_rate', lr)
  ap_val = tf.get_variable(initializer=tf.zeros_initializer, name='ap_val', shape=[], trainable=False)
  tf.summary.scalar('validation_AUC', ap_val)

  # Initialize new layers and auxiliary variables.
  init_op = tf.variables_initializer([lr, global_step, ap_val])
  sess.run(init_op)

  # Train op: we have two optimizers for two sets of weights (pretrained, new)
  opt_newlayers = tf.train.GradientDescentOptimizer(lr, name='newlayers_opt')
  grads_newlayers = tf.gradients(loss, new_layers, name='')
  trainop_newlayers = opt_newlayers.apply_gradients(zip(grads_newlayers, new_layers), global_step=global_step)
  if FLAGS.pretrained_learning_rate != 0 and len(pretrained_layers) > 0:
    grads_pretrained = tf.gradients(loss, pretrained_layers, name='')
    opt_pretrained = tf.train.GradientDescentOptimizer(FLAGS.pretrained_learning_rate, name='pretrained_opt')
    #print(grads_pretrained, pretrained_layers)
    trainop_pretrained = opt_pretrained.apply_gradients(zip(grads_pretrained, pretrained_layers),
                                                        global_step=global_step)
    train_op = tf.group(trainop_newlayers, trainop_pretrained)
  else:
    train_op = trainop_newlayers
    grads_pretrained = None

  # Add histograms for the new layers' weights and gradients.
  if FLAGS.save_weight_histograms:
    for var in new_layers:
      tf.summary.histogram(var.op.name, var)
    for grad in grads_newlayers:
      tf.summary.histogram(grad.op.name, grad)

    if FLAGS.pretrained_learning_rate != 0:
      # Add histograms for the pretrained layers as well
      for var in pretrained_layers:
        tf.summary.histogram(var.op.name, var)
      for grad in grads_pretrained:
        tf.summary.histogram(grad.op.name, grad)

  # Instantiate a SummaryWriter to output summaries and the Graph.
  summary_writer = tf.summary.FileWriter('%s' % FLAGS.log_dir, sess.graph)

  # Build the summary operation based on the TF collection of Summaries.
  summary_op = tf.summary.merge_all()

  # Create a saver for writing training checkpoints.
  saver = tf.train.Saver(max_to_keep=1)

  # And then after everything is built, start the training loop.
  start_time = time.time()
  if FLAGS.max_hours == -1:
    FLAGS.max_hours = np.inf

  # Output variables to easily compare different training runs
  d_interesting_outputs = {'best_ap': 0, 'NaNLoss': False}
  last_acc_milestone = .1

  early_stop = False
  stage = 0
  step = 0

  closeby_matrices, faraway_matrices = inputDatasets.create_or_load_neighborhood_lists(train_dset_names, train_poses, batching_params)

  print('Starting stage', stage)
  ixs_train = inputDatasets.minibatch_picker(train_poses, train_dset_ratios, batching_params,
                                             closeby_matrices, faraway_matrices)

  sess.run(training_iterator_initializer, feed_dict={train_init_ixs: ixs_train})
  while step < FLAGS.max_steps:
    # Set the learning rate
    if FLAGS.lr_policy == 'constant':
      lr_value = FLAGS.learning_rate
    elif FLAGS.lr_policy == 'exponential':
      lr_value = FLAGS.learning_rate * np.power(FLAGS.exp_base, stage)
    else:
      raise ValueError

    # LR warmup
    if step < FLAGS.slow_warmup:
      lr_value = lr_value * (step + 1) / FLAGS.slow_warmup

    try:
      # Run a step of training
      t0 = time.time()
      if FLAGS.save_sample_triplets:
        _, loss_value, loss_el, a, b, c, indexes, summary_str = sess.run(
          [train_op, loss, element_loss, batch_images, valid_triplets, nonzero_triplets, triplet_indexes, summary_op],
          feed_dict={lr: lr_value, is_training: True, handle: training_handle})
        a = ((a+1)/2*255)
        a = a.astype(np.int)
        extra.plot_triplets(a, b, c, FLAGS.log_dir + '/triplets', step, save_zero_triplets=True)
        # Show the indexes triplets
        # _, loss_value, loss_el, a, b, c, indexes, bi, paths, summary_str = sess.run([train_op, loss, element_loss, batch_images,  valid_triplets, nonzero_triplets, triplet_indexes, batch_indexes, all_train_paths, summary_op], feed_dict={lr: lr_value, is_training: True, handle: training_handle})
        # extra.plot_index_triplets(a, indexes, loss_el, paths, bi, FLAGS.log_dir+'/triplets', step)
      else:
        _, loss_value, loss_el, c, indexes, summary_str = sess.run(
          [train_op, loss, element_loss, nonzero_triplets, triplet_indexes, summary_op],
          feed_dict={lr: lr_value, is_training: True, handle: training_handle})

      if FLAGS.do_hn_mining:
        # Selecting the non zero triplet and losses
        non_zero_ind = np.squeeze(indexes[c, :])
        non_zero_losses = np.squeeze(loss_el[c])

        # Non-elegant avoiding of zeros
        if step == 0 and not any(c):
          non_zero_losses = np.zeros([])
          non_zero_ind = np.zeros([3])

        if len(non_zero_losses.shape) == 0:
          non_zero_losses = np.array([non_zero_losses])
          non_zero_ind = np.expand_dims(non_zero_ind, axis=0)

        if len(non_zero_losses) > 0:
          # Mixing cache and values
          mix_indexes = np.vstack([non_zero_ind, hn_indexes[:FLAGS.batch_size, :]])
          mix_losses = np.concatenate([non_zero_losses, hn_losses[:FLAGS.batch_size]])

        # Sorting values
        ind_sort = mix_losses.argsort()
        mix_indexes = mix_indexes[ind_sort, :]
        mix_losses = mix_losses[ind_sort]

        # Introducing values on cache
        hn_indexes[:FLAGS.batch_size, :] = mix_indexes[-FLAGS.batch_size:, :]
        hn_losses[:FLAGS.batch_size] = mix_losses[-FLAGS.batch_size:]

        # Sorting cache
        ind_sort = hn_losses.argsort()
        hn_indexes = hn_indexes[ind_sort, :]
        hn_losses = hn_losses[ind_sort]

      t1 = time.time()
    except tf.errors.OutOfRangeError:
      stage += 1
      print('Starting stage {} for step {}'.format(stage, step))

      if FLAGS.do_hn_mining:
        print('Creating HN matrix')
        new_indexes = []
        for i, tr_len in enumerate(train_lengths):
          if i == 0:
            tr_ind = hn_indexes[np.logical_and(hn_indexes[:, 0] < tr_len, hn_indexes[:, 0] >= i)]
          else:
            tr_ind = hn_indexes[
              np.logical_and(hn_indexes[:, 0] < tr_len, hn_indexes[:, 0] >= train_lengths[i - 1])]

          slicing = []
          for point in range(tr_ind.shape[0]):
            slicing += list(tr_ind[point, :])
            slicing = list(set(slicing))
            remaining_el = FLAGS.batch_size - len(slicing)

            if remaining_el == 0:
              new_indexes += slicing
              slicing = []
            elif remaining_el in [1, 2]:
              rem_list = []
              for rel in range(remaining_el):
                rem_list.append(randint(np.min(slicing), np.max(slicing)))
              slicing += rem_list
              new_indexes += slicing
              slicing = []
            elif point == tr_ind.shape[0] - 1:
              rem_list = []
              for rel in range(remaining_el):
                rem_list.append(randint(np.min(slicing), np.max(slicing)))
              slicing += rem_list
              new_indexes += slicing
              slicing = []

      ixs_train = inputDatasets.minibatch_picker(train_poses, train_dset_ratios, batching_params,
                                                 closeby_matrices,
                                                 faraway_matrices)

      if FLAGS.do_hn_mining:
        ixs_train[:len(new_indexes)] = new_indexes

        del hn_indexes
        del hn_losses
        hn_indexes = np.zeros([FLAGS.hn_cache_size, 3], dtype=np.int32)
        hn_losses = np.zeros([FLAGS.hn_cache_size], dtype=np.float32)

      sess.run(training_iterator_initializer, feed_dict={train_init_ixs: ixs_train})

      continue

    steptime = t1 - t0
    walltime = t1 - start_time
    timeout = (walltime + 300) / 3600.0 > FLAGS.max_hours  # timeout 5 minutes before the maximum

    # Evaluate
    best_ap = False
    if step % FLAGS.validation_steps == 0 and step > 0 or step == 0 and FLAGS.do_initial_eval or step + 1 == FLAGS.max_steps or timeout:
      print("eval: collecting embeddings from", FLAGS.validation_set)
      batch_eval = 0
      sess.run(validation_iterator_initializer)
      while True:  # all the validation dataset
        try:
          s_ix = batch_eval * FLAGS.batch_size
          e_ix = (batch_eval + 1) * FLAGS.batch_size
          if e_ix > eff_len_val:
            e_ix = eff_len_val
          t, tt, summary_str = \
            sess.run([embeddings, batch_poses, summary_op], {is_training: False, handle: validation_handle})
          eval_embeddings[s_ix:e_ix, ...] = t[0: e_ix - s_ix, ...]
          eval_poses[s_ix:e_ix, ...] = tt[0: e_ix - s_ix, ...]
          batch_eval += 1
        except tf.errors.OutOfRangeError:
          break
      if FLAGS.validation_set_2 != "":  # 2-dataset evaluation
        print("eval: collecting embeddings from", FLAGS.validation_set_2)
        batch_eval = 0
        sess.run(val2_iterator_initializer)
        while True:  # all the validation dataset
          try:
            s_ix = batch_eval * FLAGS.batch_size
            e_ix = (batch_eval + 1) * FLAGS.batch_size
            if e_ix > eff_len_val2:
              e_ix = eff_len_val2
            t, tt, summary_str = \
              sess.run([embeddings, batch_poses, summary_op], {is_training: False, handle: validation_2_handle})
            eval_embeddings_2[s_ix:e_ix, ...] = t[0: e_ix - s_ix, ...]
            eval_poses_2[s_ix:e_ix, ...] = tt[0: e_ix - s_ix, ...]
            batch_eval += 1
          except tf.errors.OutOfRangeError:
            break

        # Here the validation is result of crossing between the validation sets
        confmat = cdist(eval_embeddings, eval_embeddings_2)
        eucdist = cdist(eval_poses[:, :3], eval_poses_2[:, :3])
        quatdist = inputDatasets.quat_dist(eval_poses[:, 3:], eval_poses_2[:, 3:])
        confmat_2dim = confmat #confmat is a 2 dimensional array because we use different datasets
        eucdist_2dim = eucdist
        quatdist_2dim = quatdist

      else:
        confmat = pdist(eval_embeddings)
        eucdist = pdist(eval_poses[:, :3])
        quatdist = inputDatasets.quat_dist(eval_poses[:, 3:], eval_poses[:, 3:])
        np.fill_diagonal(quatdist, 0.0)
        quatdist = squareform(quatdist)
        confmat_2dim = squareform(confmat) #convert the compact distance matrix into a square matrix for display
        #eucdist_2dim = squareform(eucdist)
        #quatdist_2dim = squareform(quatdist)

      print("eval: calculating PR curve and AP")
      GT = np.logical_and(eucdist < FLAGS.tol_t, quatdist < tol_quat)  # our ground truth
      P, R, _ = precision_recall_curve(GT, -1 * confmat) # reverse confmat to get a 'similarity score'
      average_precision = average_precision_score(GT, -1 * confmat)
      if GT.ndim == 2:
        GT_2dim = GT
      else:
        GT_2dim = squareform(GT)
      ap_val.load(average_precision, session=sess)

      if P.shape[0] > 1000:
        ixs_PR = np.linspace(0, P.shape[0]-1, num=1000, dtype=np.int)
      else:
        ixs_PR = np.arange(P.shape[0])
      P = P[ixs_PR]
      R = R[ixs_PR]

      # Save the precision-recall curve
      plt.figure()
      extra.plot_pr_curve(R, P)
      plt.title("step {}, AP: {}".format(step, average_precision))
      plt.savefig(os.path.join(FLAGS.log_dir, 'prc-{:0>6d}.jpg'.format(step)))

      # Save the confmat as an image
      plt.clf()
      plt.imshow(confmat_2dim)
      plt.colorbar()
      plt.savefig(os.path.join(FLAGS.log_dir, 'confmat-{:0>6d}.jpg'.format(step)))

      # Save the GT as an image
      if step == 0:
        plt.clf()
        plt.imshow(GT_2dim)
        plt.savefig(os.path.join(FLAGS.log_dir, 'GT.jpg'.format(step)))

      # Save a histogram of the norm of the embeddings
      if False:
        plt.clf()
        plt.hist(np.linalg.norm(eval_embeddings, axis=1), bins=30, normed=False)
        plt.savefig(os.path.join(FLAGS.log_dir, 'descriptornorm-{:0>6d}.jpg'.format(step)))

      plt.clf()
      plt.close('all')

      if False:  # Save h5 files with the confmats for debugging / visualizing the validation process
        path_confmat = os.path.join(FLAGS.log_dir, 'confmat_eval-{:0>6d}.h5'.format(step))
        print("eval: saving eval matrices in {}".format(path_confmat))
        with h5py.File(path_confmat, 'w') as h5f:
          h5f['N'] = len(eval_embeddings)
          h5f['confmat'] = confmat
          h5f['eucdist'] = eucdist
          h5f['quatdist'] = quatdist
          # h5f['eval_embeddings'] = eval_embeddings

      # Record accuracy milestones
      d_interesting_outputs["last_ap"] = average_precision
      if average_precision > d_interesting_outputs["best_ap"]:
        best_ap = True
        d_interesting_outputs["best_ap"] = average_precision
        while average_precision > last_acc_milestone + .1:
          last_acc_milestone += .1
          d_interesting_outputs["t_acc_" + str(int(last_acc_milestone * 100))] = walltime
          d_interesting_outputs["step_acc_" + str(int(last_acc_milestone * 100))] = step

      # Stop early if learning is not going well
      if d_interesting_outputs["best_ap"] > 0.1 > average_precision:
        print("Stopping due to low AP")
        early_stop = True

      # Save the model at the best AP
      if best_ap and average_precision > 0.6:
        p1 = os.path.join(FLAGS.snapshot_dir, 'snap_best_ap')
        if os.path.exists(p1):
          os.rmdir(p1)
        print('Saving into', p1)
        saver.save(sess, p1, global_step=step)
      gc.collect()
      # =========== END OF EVAL =========== #

    d_interesting_outputs["last_step"] = step
    d_interesting_outputs["last_time"] = walltime

    # Save a checkpoint
    if False:  # step % 1000 == 0 or step+1 == FLAGS.max_steps or timeout:
      print('Saving into', FLAGS.snapshot_dir)
      saver.save(sess, FLAGS.snapshot_dir + '/snap', global_step=step)

    # Write the summaries and print an overview fairly often.
    if step % 100 == 0 or step + 1 == FLAGS.max_steps or timeout or early_stop:
      summary_writer.add_summary(summary_str, step)
      summary_writer.flush()

    # Stop based on time
    if timeout:
      print("Stopping due to timeout of {} hours".format(FLAGS.max_hours))
      break

    if step % 10 == 0:
      print('Step %d: loss = %.5f, (step: %.2f sec), (%.2f h)' % (
        step, loss_value, steptime, walltime / 3600))

    if np.isnan(loss_value):
      print("Loss is NaN, stopping.")
      d_interesting_outputs["NaNLoss"] = True
      early_stop = True

    with open(FLAGS.snapshot_dir + '/interesting_outputs.json', 'w') as f:
      json.dump(d_interesting_outputs, f)

    if early_stop:
      break

    step += 1

  # End of training
  with open(FLAGS.snapshot_dir + '/interesting_outputs.json', 'w') as f:
    json.dump(d_interesting_outputs, f)


def main(_):
  # Evaluate with the starting weights
  run_training()


if __name__ == '__main__':
  tf.app.run()
