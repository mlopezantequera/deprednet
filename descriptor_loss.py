import tensorflow as tf
import numpy as np

def quat_dist(q1, q0, diagonal_only = False):
  """
  Rotation distance using quaternions. Robust to non-sign-normalized quaternions.
  :param q1: tensor of size (N, 4)
  :param q2: tensor of size (N, 4)
  :return: tensor of size (N,N) if pairwise=True, size N (just the diagonal) if diagonal_only=False
  """
  if diagonal_only:
    return (tf.subtract(1.0, tf.square(tf.reduce_sum(tf.multiply(q1, q0), axis=1))))
  else:
    return (tf.subtract(1.0, tf.square(tf.matmul(q1, q0, transpose_b=True))))


def kernel_loss(embeddings, poses, sigma_t, sigma_r):
  with tf.name_scope('kernel_loss'):
    # From a batch size of N embeddings, we calculate the loss over all n(n-1)/2 possible combinations
    combix = np.triu_indices(int(embeddings.shape[0]), 1)
    ixs_a, ixs_b = combix[0].astype(np.int32), combix[1].astype(np.int32)
    emb1 = tf.gather(embeddings, ixs_a, name='embs_a')  # embeddings[ixs_a, ...]
    emb2 = tf.gather(embeddings, ixs_b, name='embs_b')  # embeddings[ixs_b, ...]
    poses1 = tf.gather(poses, ixs_a, name='poses_a')  # poses[ixs_a, ...]
    poses2 = tf.gather(poses, ixs_b, name='poses_b')  # poses[ixs_b, ...]

    ssd_a = tf.reduce_sum(tf.squared_difference(emb1, emb2), 1)
    ssd_t = tf.reduce_sum(tf.squared_difference(poses1[:, :3], poses2[:, :3]), 1) #xyz squared euclidean
    ssd_r = tf.reduce_sum(tf.squared_difference(poses1[:, 3:], poses2[:, 3:]), 1) #quaternion squared euclidean

    kernel_a = tf.exp(-ssd_a)
    kernel_pose = tf.cast(tf.exp(- 0.5 * ssd_t / sigma_t ** 2) * tf.exp(- 0.5 * ssd_r / sigma_r ** 2), tf.float32)

    loss = tf.reduce_mean(tf.squared_difference(kernel_pose, kernel_a))
  raise NotImplementedError("This loss still uses quaternion norms which could be problematic")
  return loss, tf.sqrt(ssd_a, name = 'eucd'), kernel_a, kernel_pose

def ixs_valid_triplets(bsz):
  # From a batch size of N embeddings, we calculate the loss over all n(n-1)(n-2)/2 possible triplets
  ixs_a, ixs_b, ixs_c = [], [], []
  for a in range(bsz):
    for b in range(a, bsz): #We don't want to repeat triplets a,b,c and b,a,c
      for c in range(bsz):
        if a != b and a != c and b != c:
          ixs_a.append(a)
          ixs_b.append(b)
          ixs_c.append(c)

  return ixs_a, ixs_b, ixs_c

def triplet_loss(embeddings, poses, indexes, th_t, th_r_quat, min_t_distant, min_r_distant_quat, margin):
  with tf.name_scope('triplet_loss'):
    ixs_a, ixs_b, ixs_c = ixs_valid_triplets(embeddings.shape[0])

    emba = tf.gather(embeddings, ixs_a, name='embs_a')
    embb = tf.gather(embeddings, ixs_b, name='embs_b')
    embc = tf.gather(embeddings, ixs_c, name='embs_c')
    posesa = tf.gather(poses, ixs_a, name='poses_a')
    posesb = tf.gather(poses, ixs_b, name='poses_b')
    posesc = tf.gather(poses, ixs_c, name='poses_c')
    inda = tf.gather(indexes, ixs_a, name='indexes_a')
    indb = tf.gather(indexes, ixs_b, name='indexes_b')
    indc = tf.gather(indexes, ixs_c, name='indexes_c')

    norm_tab = tf.norm(posesa[:, :3] - posesb[:, :3], axis=1) #xyz euclidean
    norm_tac = tf.norm(posesa[:, :3] - posesc[:, :3], axis=1)  # xyz euclidean
    dist_rab = quat_dist(posesa[:, 3:], posesb[:, 3:], diagonal_only=True)
    dist_rac = quat_dist(posesa[:, 3:], posesc[:, 3:], diagonal_only=True)

    # Valid triplets are those where (a,b) is a similar pair and (a,c) are dissimilar
    # We use 2 different thresholds for matches and distant images, to eliminate edge cases.
    ab_match = tf.logical_and(tf.less(norm_tab, th_t), tf.less(dist_rab, th_r_quat))
    ac_notmatch = tf.logical_or(tf.greater(norm_tac, min_t_distant), tf.greater(dist_rac, min_r_distant_quat))
    valid_triplets = tf.cast(tf.logical_and(ab_match, ac_notmatch),tf.float32)
    n_valid_triplets = tf.reduce_sum(valid_triplets)

    d_pos = tf.reduce_sum(tf.squared_difference(emba, embb), 1)
    d_neg = tf.reduce_sum(tf.squared_difference(emba, embc), 1)
    diff = d_pos - d_neg
    loss = tf.maximum(0., margin + diff)
    if True:
      # Workaround for TF bug: https://github.com/tensorflow/tensorflow/issues/12659
      loss = tf.where(tf.is_nan(diff), np.zeros(loss.shape, dtype=np.float32) + np.nan, loss)
    element_loss = loss * valid_triplets  # we calculated some invalid loss components, we simply zero them out
    triplet_matrix = tf.stack([inda, indb, indc], axis=-1)

    nonzero_triplets = tf.greater(element_loss, 0)
    n_nonzero_triplets = tf.count_nonzero(element_loss, dtype = tf.float32)
    loss = tf.reduce_sum(element_loss)
    if False: # A
      # we average only over triplets which have a nonzero loss.
      loss = tf.where(tf.logical_or(n_nonzero_triplets > 0, tf.is_nan(loss)), tf.divide(loss, n_nonzero_triplets), 0.0, name='loss')
    else: #C
      # we average over all valid triplets.
      # This yields NaN only if there was NaN in the individual loss entries, (not if n_valid_triplets is 0).
      loss = tf.where(tf.logical_or(n_valid_triplets > 0, tf.is_nan(loss)), tf.divide(loss, n_valid_triplets), 0.0, name='loss')

    tf.summary.scalar('nonzero_triplets', n_nonzero_triplets)
    tf.summary.scalar('valid_triplets', n_valid_triplets)

    """
      TODO: When there are no valid triplets, we return 0.0 loss, which destabilizes the optimizer yielding NaN gradients.
      This happens when setting thresholds too low (can't find valid triplets)
      It will also happen if you try to use loss division 'A' instead of B or C.
    """
  return loss, element_loss, valid_triplets, nonzero_triplets, triplet_matrix

def contrastive_loss(embeddings, poses, th_t, th_r, margin):
  #todo update me to have a third label: unknown (we can have then 2 different thresholds to define far away or closeby)
  with tf.name_scope('contrastive_loss'):
    # From a batch size of N embeddings, we calculate the loss over all n(n-1)/2 possible combinations
    combix = np.triu_indices(int(embeddings.shape[0]), 1)
    ixs_a, ixs_b = combix[0].astype(np.int32), combix[1].astype(np.int32)
    emb1 = tf.gather(embeddings, ixs_a, name='embs_a')  # embeddings[ixs_a, ...]
    emb2 = tf.gather(embeddings, ixs_b, name='embs_b')  # embeddings[ixs_b, ...]
    poses1 = tf.gather(poses, ixs_a, name='poses_a')  # poses[ixs_a, ...]
    poses2 = tf.gather(poses, ixs_b, name='poses_b')  # poses[ixs_b, ...]

    ssd_a = tf.reduce_sum(tf.squared_difference(emb1, emb2), 1)
    norm_t = tf.norm(poses1[:, :3] - poses2[:, :3], axis=1) #xyz euclidean
    norm_r = tf.norm(poses1[:, 3:] - poses2[:, 3:], axis=1) #quaternion euclidean

    label = tf.cast(tf.logical_and(tf.less(norm_t, th_t), tf.less(norm_r, th_r)), tf.float32) # 1 if 'similar pair', 0 if 'different pair'
    tf.summary.histogram('label', label)
    tf.summary.histogram('norm_t', norm_t)
    tf.summary.histogram('norm_r', norm_r)
    loss = label * tf.square(tf.maximum(0., margin - tf.sqrt(ssd_a))) + (1 - label) * ssd_a
    loss = tf.multiply(.5, tf.reduce_mean(loss), name = 'loss')
  raise NotImplementedError("This loss still uses quaternion norms which could be problematic")
  return loss

def triplet_loss_2(embeddings, preselected_triplets, margin):
  """Triplet loss

  :param embeddings: embeddings from a minibatch of size n
  :param preselected_triplets: 3-tuple of indices for all the valid triplets in this minibatch.
  :param margin
  :return: loss
  """
  with tf.name_scope('triplet_loss'):
    ixs_a, ixs_b, ixs_c = preselected_triplets

    emba = tf.gather(embeddings, ixs_a, name='embs_a')
    embb = tf.gather(embeddings, ixs_b, name='embs_b')
    embc = tf.gather(embeddings, ixs_c, name='embs_c')

    d_pos = tf.reduce_sum(tf.squared_difference(emba, embb), 1)
    d_neg = tf.reduce_sum(tf.squared_difference(emba, embc), 1)
    loss = tf.maximum(0., margin + d_pos - d_neg)

    nonzero_triplets = tf.greater(loss, 0)
    n_nonzero_triplets = tf.count_nonzero(loss, dtype = tf.float32)
    if False:
      # we average only over triplets which have a nonzero loss.
      loss = tf.where(n_nonzero_triplets > 0, tf.divide(tf.reduce_sum(loss), n_nonzero_triplets), 0.0, name='loss')  # avoid a divide by 0
    elif False:
      # we could try something like this to 'soften' the effect of a batch with very few triplets
      # (which might be very hard / too hard / bad triplets)
      min_nonzero = 10
      loss = tf.divide(tf.reduce_sum(loss), tf.where(n_nonzero_triplets > min_nonzero, n_nonzero_triplets, min_nonzero))
    else:
      # we average over all triplets.
      loss = tf.reduce_mean(loss)
    tf.summary.scalar('nonzero_triplets', n_nonzero_triplets)

  return loss, nonzero_triplets


def contrastive_loss_2(embeddings, labels, margin):
  """Contrastive loss

  :param embeddings: embeddings from a minibatch of size n
  :param labels: boolean for similar/dissimilar label for each possible pair n(n-1)/2
  :param margin
  :return: loss
  """
  with tf.name_scope('contrastive_loss'):
    # From a batch size of N embeddings, we calculate the loss over all n(n-1)/2 possible combinations
    combix = np.triu_indices(int(embeddings.shape[0]), 1)
    ixs_a, ixs_b = combix[0].astype(np.int32), combix[1].astype(np.int32)
    emb1 = tf.gather(embeddings, ixs_a, name='embs_a')
    emb2 = tf.gather(embeddings, ixs_b, name='embs_b')
    ssd_a = tf.reduce_sum(tf.squared_difference(emb1, emb2), 1)
    tf.summary.histogram('label', labels)
    loss = labels * tf.square(tf.maximum(0., margin - tf.sqrt(ssd_a))) + (1 - labels) * ssd_a
    loss = tf.multiply(.5, tf.reduce_mean(loss), name = 'loss')
  return loss
'''
def proportional_siamese_loss(emb1, emb2, expected_distance, margin):
  """
  loss for siamese nets

  expected_distance is some scalar defining an 'expected' distance between two embeddings.
  If expected_distance > margin, d_app only needs to be >= margin, but not necessarily equal to the expected distance.

  There is a drawing of this cost function in proportional_siamese_loss_drawing.svg
  """
  with tf.name_scope('proportional_siamese_loss'):
    # todo rewrite using tf.norm
    # d_app = tf.norm(emb1-emb2, ord='euclidean', axis = 1, name = 'eucd')
    d_app = tf.sqrt(tf.reduce_sum(tf.squared_difference(emb1, emb2), 1), name = 'eucd')
    distant_pair  = tf.cast(tf.greater(expected_distance, margin), tf.float32)
    mask = tf.cast(tf.logical_or(tf.less(expected_distance, margin), tf.less(d_app, margin)), tf.float32)
    num_pairs_in_mask = tf.reduce_sum(mask)
    tf.summary.scalar('num_pairs_in_mask',num_pairs_in_mask)
    loss = tf.reduce_mean(tf.multiply(mask, tf.squared_difference(expected_distance, d_app)), name ='loss')
    #todo CUIDADO CUANDO LA SUMA DE LA MASCARA SEA 0! (que en teoria nunca pasa con mi sampleo de minibatches, pero bueno...)
    #loss = tf.divide(tf.reduce_sum(tf.multiply(mask, tf.squared_difference(expected_distance, d_app))), num_pairs_in_mask, name ='loss')
  return loss, d_app, mask, distant_pair
'''