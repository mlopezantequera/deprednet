import os
import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from random import shuffle

from descriptor_loss import ixs_valid_triplets

def plot_pr_curve(R, P, label = None):
  #plt.hlines(np.sum(GT) / GT.size, 0, 1, colors='r', linestyles='--', alpha=.5, label='random') #random baseline
  step = plt.step(R, P, color='b', alpha=0.5, where='post', label = label)
  plt.fill_between(R, P, step='post', alpha=0.2, color='b')
  plt.ylabel('precision')
  plt.xlabel('recall')
  return step

def plot_triplet(image_batch, ixs, path):
  fig = plt.figure()
  plt.axis('off')
  for im_ix, plot_ix in zip(ixs,(1,2,3)):
    ax = plt.subplot(1,3,plot_ix)
    ax.imshow(image_batch[im_ix].astype(np.uint8))
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
  fig.savefig(path, bbox_inches='tight', pad_inches=0)
  plt.close(fig)

def plot_triplets(image_batch, valid_triplets, nonzero_triplets, path, step,
                  max_triplets_per_step = 3, max_triplets_to_save = 100, save_zero_triplets = False):
  bsz = image_batch.shape[0]
  ix = 0
  ixs_nonzero_triplets = []
  ixs_zero_triplets = []
  ixs_a,ixs_b,ixs_c = ixs_valid_triplets(bsz)
  for a,b,c in zip(ixs_a,ixs_b,ixs_c):
    if valid_triplets[ix]:
      if nonzero_triplets[ix]:
        ixs_nonzero_triplets.append((a, b, c))  # both conditions must be met
      else:
        if save_zero_triplets:
          ixs_zero_triplets.append((a, b, c))  # both conditions must be met
    ix+=1

  for ixs_triplets, subdir in zip ((ixs_zero_triplets, ixs_nonzero_triplets), ("zero", "nonz")):
    # draw and save new triplets
    shuffle(ixs_triplets)
    for plot_ix, triplet in enumerate(ixs_triplets[:max_triplets_per_step]):
      plot_triplet(image_batch, triplet, os.path.join(path, subdir, "{}-{}.jpg".format(step, plot_ix)))

    # clear old triplets.
    files = [os.path.join(path,subdir,f) for f in os.listdir(os.path.join(path, subdir))]
    files = sorted(files, key=os.path.getctime)
    if len(files) > max_triplets_to_save:
      for f in files[:-max_triplets_to_save]:
        os.remove(os.path.join(path, subdir, f))

def plot_index_triplet(image_batch, impaths, indexes, path, loss):
  fig = plt.figure()
  fig.suptitle('LOSS: ' +str(loss) + '\nPath: ' + '/'.join(impaths[0].decode("utf-8").split('/')[:-1]))
  plt.axis('off')
  for plot_ix, pa, pa1 in zip((1,2,3), indexes[0], indexes[1]):
    ax = plt.subplot(1,3,plot_ix)
    ax.set_title(impaths[pa1].decode("utf-8").split('/')[-1], fontsize=9)
    ax.imshow(image_batch[pa].astype(np.uint8))
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
  fig.savefig(path, bbox_inches='tight', pad_inches=0)
  plt.close(fig)

def plot_index_triplets(image_batch, triplet_indexes, element_loss, im_paths, batch_indexes, path, step):
  bsz = image_batch.shape[0]
  ix = 0
  print('TRIPLET_INDEXES', triplet_indexes)
  print('BATCH INDEXES', batch_indexes)
  print('LOSS', element_loss)
  print(np.count_nonzero(element_loss))
  print(len(element_loss), triplet_indexes.shape)

  for f in os.listdir(os.path.join(path, 'indexes')):
    os.remove(os.path.join(path, 'indexes', f))

  ixs_a, ixs_b, ixs_c = ixs_valid_triplets(bsz)
  ixs = zip(ixs_a,ixs_b,ixs_c)
  exit()

  tam = 0
  for x in range(triplet_indexes.shape[0]):
    # a = np.argwhere(batch_indexes==triplet_indexes[x, 0])[0][0]
    if element_loss[x] > 0 and tam < 30:
      a = np.argwhere(batch_indexes==triplet_indexes[x, 0])[0][0]
      b = np.argwhere(batch_indexes==triplet_indexes[x, 1])[0][0]
      c = np.argwhere(batch_indexes==triplet_indexes[x, 2])[0][0]
      print(x, a, b, c)
      plot_index_triplet(image_batch, im_paths, [(a,b,c),(triplet_indexes[x, 0],triplet_indexes[x, 1],triplet_indexes[x, 2])], os.path.join(path, 'indexes', "{}{}{}-{}.jpg".format(a, b, c, element_loss[x])),element_loss[x])
      tam += 1


