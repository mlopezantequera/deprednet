#!/usr/bin/env python3

import os
import argparse
import json
import h5py
import numpy as np
import tensorflow as tf
import scipy.misc
from time import time

dir(tf.contrib)  # github.com/tensorflow/tensorflow/issues/10130

"""

This file loads a full graph checkpoint and extracts embeddings from it. 
(It does not require a network definition file)
 
Images are resized and fed to the network. No other preprocessing is done. Watch out for this!

"""


def read_images(image_dir, image_paths, image_height, image_width, startix, endix, output_array):
  ix = 0
  for file in image_paths[startix:endix]:
    image_file = os.path.join(image_dir, file)
    assert os.path.exists(image_file), \
      "File does not exist: %s" % image_file
    image = scipy.misc.imread(image_file, mode='RGB')
    output_array[ix, ...] = scipy.misc.imresize(image, size=(image_height, image_width), interp='cubic')
    ix += 1
  return output_array


def do_dset(weights_fp, index_fp, input_tensor_name='input_images:0', output_tensor_name='descript_out:0'):
  config = tf.ConfigProto(log_device_placement=False)
  config.gpu_options.allow_growth = True
  sess = tf.Session(config=config)

  dset_name = index_fp.split('/')[-1].split('.')[0]
  weightsname = os.path.split(weights_fp)[1]
  output_fp = "/".join(index_fp.split('/')[:-1]) + '/embeddings_' + weightsname + '_' + dset_name + '.h5'
  meta_fp = weights_fp + '.meta'

  saver = tf.train.import_meta_graph(meta_fp)
  saver.restore(sess, weights_fp)
  graph = tf.get_default_graph()

  embeddings = graph.get_tensor_by_name(output_tensor_name)
  inputs = graph.get_tensor_by_name(input_tensor_name)
  outdim = int(embeddings.shape[1])
  batch_size = int(embeddings.shape[0])
  image_height = int(inputs.shape[1])
  image_width = int(inputs.shape[2])
  inputs_val = np.zeros((batch_size, image_height, image_width, 3))

  with open(index_fp, 'r') as f:
    d = json.load(f)

  image_root = os.path.join(os.path.split(index_fp)[0], d['im_prefix'])
  image_paths = d['im_paths'][:]
  len_dataset = len(image_paths)
  full_batches_in_dataset = len_dataset // batch_size
  leftover_imgs = len_dataset - full_batches_in_dataset * batch_size

  with h5py.File(output_fp, 'w') as outf:
    outf.create_dataset('features', (len_dataset, outdim), 'float32')
    for j in range(full_batches_in_dataset):
      s_ix = j * batch_size
      e_ix = (j + 1) * batch_size
      t0 = time()
      read_images(image_root, image_paths, image_height, image_width, s_ix, e_ix, inputs_val)
      t1 = time()
      embeddings_val = sess.run(embeddings, feed_dict={inputs: inputs_val, 'is_training:0': 'False'})
      t2 = time()
      t_load = t1 - t0
      t_forwardpass = t2 - t1
      outf['features'][s_ix:e_ix, ...] = embeddings_val
      if j % 10 == 0:
        print("Batch {}/{} ({:.2%}) load: {:.1f} ms/im, pass: {:.1f} ms/im".format(
          j, full_batches_in_dataset, j / full_batches_in_dataset, 1000 * t_load / batch_size,
                                      1000 * t_forwardpass / batch_size))
    if leftover_imgs > 0:
      print("Leftover batch...")
      read_images(image_root, image_paths, image_height, image_width, len_dataset - batch_size, len_dataset,
                  inputs_val)
      embeddings_val = sess.run(embeddings, feed_dict={inputs: inputs_val, 'is_training:0': 'False'})
      outf['features'][len_dataset - batch_size:, ...] = embeddings_val

  print("Done!, saved output in: {}".format(output_fp))
  sess.close()


if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('path_weights', help='Path and name of a snapshot such as /path/to/snap[.meta/.index.data]')
  parser.add_argument('path_dataset_index', help='Path to a .json file for the dataset')
  parser.add_argument('-in_t', help='Name of the input tensor', default='input_images:0')
  parser.add_argument('-out_t', help='Name of the output tensor', default='descript_out:0')
  args = parser.parse_args()
  do_dset(args.path_weights, args.path_dataset_index, input_tensor_name=args.in_t, output_tensor_name=args.out_t)
